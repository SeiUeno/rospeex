from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

setup_args = generate_distutils_setup(
    packages=['rospeex_if'],
    package_dir={'': 'src/py'}
)

setup(**setup_args)
