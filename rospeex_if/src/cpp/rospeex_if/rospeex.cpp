#include "rospeex_if/rospeex.h"

#include <stdio.h>
#include <stdint.h>
#include <sstream>

#include <sys/stat.h>

#include <ros/ros.h>
#include <ros/console.h>
#include <ros/package.h>

#include <boost/format.hpp>
#include <boost/filesystem.hpp>

// include messages
#include "rospeex_msgs/SpeechSynthesisResponse.h"
#include "rospeex_msgs/SpeechSynthesisRequest.h"
#include "rospeex_msgs/SpeechSynthesisHeader.h"
#include "rospeex_msgs/SpeechSynthesisState.h"
#include "rospeex_msgs/SpeechRecognitionResponse.h"
#include "rospeex_msgs/SpeechRecognitionRequest.h"
#include "rospeex_msgs/SignalProcessingResponse.h"
#include "rospeex_msgs/SpeechRecognitionConfig.h"

using namespace rospeex;
using namespace rospeex_msgs;

#define SR_RESPONSE_TOPIC_NAME "sr_res"
#define SS_RESPONSE_TOPIC_NAME "ss_res"
#define SPI_RESPONSE_TOPIC_NAME "spi_res"
#define SR_REQUEST_TOPIC_NAME "sr_req"
#define SS_REQUEST_TOPIC_NAME "ss_req"
#define SS_STATE_TOPIC_NAME "ss_state"
#define SPI_CONFIG_SERVICE_NAME "spi_config"


class Interface::Impl
{
public:
	Impl()
		: sr_func_()
		, ss_func_()
		, ss_request_id_(0)
		, sr_request_id_(0)
		, sr_pub_()
		, ss_pub_()
		, ss_state_pub_()
		, sr_sub_()
		, ss_sub_()
		, spi_sub_()
		, ss_enable_()
		, sr_enable_()
		, spi_enable_()
	{}

	/*!
	 * @brief signal processing response
	 * @param[in] response response from ros node
	 */
	void SPIResponse( const SignalProcessingResponse::ConstPtr& response );

	/*!
	 * @brief speech recognition response
	 * @param[in] response response from ros node
	 */
	void SRResponse( const SpeechRecognitionResponse::ConstPtr& response );

	/*!
	 * @brief speech synthesis response
	 * @param[in] response response from ros node
	 */
	void SSResponse( const SpeechSynthesisResponse::ConstPtr& response );

	/*!
	 * @brief get ros package path
	 * @param[in]  package_name ros package name (e.g. rospeex)
	 * @return package absolute path
	 */
	void playPackageSound( const std::string& package_name );

	/*!
	 * @brief playing audio file.
	 * @param[in] file_name  sound file name (file location: rospeex/sound)
	 */
	void playSound( const std::string& file_name );

	/*!
	 * send speech synthesis state
	 * @param[in] state speech syntnesis state
	 */
	void sendSpeechSynthesisState( bool state );

	uint32_t ss_request_id_;					//!> speech synthesis request id
	uint32_t sr_request_id_;					//!< sppech recognition request id

	SpeechRecognizeCallback sr_func_;			//!< speech recognition callback function
	SpeechSynthesisCallback ss_func_;			//!< speech synthesis callback function

	ros::Publisher sr_pub_;						//!< speech recognition publisher
	ros::Publisher ss_pub_;						//!< speech synthesis publisher
	ros::Publisher ss_state_pub_;				//!< speech synthesis state publisher

	ros::Subscriber sr_sub_;					//!< speeech recognition subscriber
	ros::Subscriber ss_sub_;					//!< speeech synthesis subscriber
	ros::Subscriber spi_sub_;					//!< signal processing interace subscriber

	std::string spi_engine_;					//!< signal processing interface engine
	std::string spi_language_;					//!< signal processing interface language

	bool ss_enable_, sr_enable_, spi_enable_;
};


void Interface::Impl::SPIResponse( const SignalProcessingResponse::ConstPtr& response )
{
	if ( sr_func_.empty() == false ) {
		playPackageSound("accept.wav");
		sr_request_id_++;

	} else {
		// Do Nothing
	}
}


void Interface::Impl::SRResponse( const SpeechRecognitionResponse::ConstPtr& response )
{

	if( response->message == "" ) {
		playPackageSound("nomessage.wav");

	} else if ( spi_enable_ != true ) {
		playPackageSound("accept.wav");
	}

	ros::NodeHandle n;
	std::string user = response->header.user;
	bool check_user = ( user == "spi" || user == n.getNamespace() );
	if ( sr_func_.empty() == false && check_user == true) {
		sr_func_( response->message );

	} else {
		// Do Nothing
	}
}


void Interface::Impl::SSResponse( const SpeechSynthesisResponse::ConstPtr& response )
{
	ros::NodeHandle n;
	if ( ss_func_.empty() == false && response->header.user == n.getNamespace() ) {
		ss_func_( response->data );

	} else {
		// Do Nothing
	}
}


Interface::Interface()
	: impl_( new Impl() )
{}


Interface::~Interface()
{}


bool Interface::init( bool ss_enable, bool sr_enable, bool spi_enable )
{
	impl_ = boost::shared_ptr<Impl>(new Impl());

	ros::NodeHandle n;
	impl_->ss_enable_ = ss_enable;
	impl_->sr_enable_ = sr_enable;
	impl_->spi_enable_ = spi_enable;

	// init speech recognize
	if ( sr_enable == true ) {
		ROS_INFO("enable speeech recognition.");
		impl_->sr_pub_ = n.advertise<SpeechRecognitionRequest>(SR_REQUEST_TOPIC_NAME, 10);
		boost::function<void(const SpeechRecognitionResponse::ConstPtr&)> sr_func = boost::bind(&Impl::SRResponse, this->impl_, _1);
		impl_->sr_sub_ = n.subscribe(SR_RESPONSE_TOPIC_NAME, 10, sr_func);
	}

	// init speech synthesis
	if ( ss_enable == true ) {
		ROS_INFO("enable speech synthsis.");
		impl_->ss_pub_ = n.advertise<SpeechSynthesisRequest>(SS_REQUEST_TOPIC_NAME, 10);
		boost::function<void(const SpeechSynthesisResponse::ConstPtr&)> ss_func = boost::bind(&Impl::SSResponse, this->impl_, _1);
		impl_->ss_sub_ = n.subscribe(SS_RESPONSE_TOPIC_NAME, 10, ss_func);
	}

	// init signal processing interface
	if ( spi_enable == true ) {
		ROS_INFO("enable signal processing interface.");
		boost::function<void(const SignalProcessingResponse::ConstPtr&)> spi_func = boost::bind(&Impl::SPIResponse, this->impl_, _1);
		impl_->ss_state_pub_ = n.advertise<SpeechSynthesisState>(SS_STATE_TOPIC_NAME, 10);
		impl_->spi_sub_ = n.subscribe(SPI_RESPONSE_TOPIC_NAME, 10, spi_func);
	}

	return true;
}


void Interface::Impl::playPackageSound( const std::string& sound_file )
{
	// set spi input off
	sendSpeechSynthesisState(true);

	// get sound path
	std::string package_path = ros::package::getPath("rospeex_if");
	boost::filesystem::path sound_path(package_path + "/sound/" + sound_file);
	playSound(sound_path.string());

	// set spi input on
	sendSpeechSynthesisState(false);
}


void Interface::Impl::sendSpeechSynthesisState( bool state )
{
	// create header
	if ( ss_state_pub_ ) {
		ros::NodeHandle n;
		SpeechSynthesisState ss_state;
		ss_state.header.language = "";
		ss_state.header.voice_font = "";
		ss_state.header.engine = "";
		ss_state.header.user = n.getNamespace();
		ss_state.header.request_id = "0";
		ss_state.header.request_type = SpeechSynthesisHeader::REQUEST_TYPE_SAY;
		ss_state.play_state = state;
		ss_state_pub_.publish(ss_state);
	}
}


void Interface::Impl::playSound( const std::string& file_path )
{
	// check file exists
	boost::system::error_code error;
	bool is_exist = boost::filesystem::exists(file_path, error);

	if ( error ) {
		ROS_ERROR("file[%s] open error. %s",file_path.c_str() ,error.message().c_str());
		return;
	}

	if ( !is_exist ) {
		ROS_ERROR("[%s] is not exist.", file_path.c_str());
		return;
	}

	// play sound if sound file exist.
	std::string command = (boost::format("aplay -q %s") % file_path).str();
	int ret = system(command.c_str());
	if(ret != 0){
		ROS_ERROR("playing sound error.");
	}
}


void Interface::say( const std::string& msg,
				const std::string& language,
				const std::string& engine,
				const std::string& voice_font)
{
	if ( impl_->ss_pub_ ) {
		ros::NodeHandle n;
		std::stringstream ss;
		ss << impl_->ss_request_id_;
		SpeechSynthesisRequest request;
		request.header.language = language;
		request.header.voice_font = voice_font;
		request.header.engine = engine;
		request.header.user = n.getNamespace();
		request.header.request_id = ss.str();
		request.header.request_type = SpeechSynthesisHeader::REQUEST_TYPE_SAY;
		request.message = msg;
		impl_->ss_pub_.publish(request);
		impl_->ss_request_id_++;

	} else {
		ROS_INFO("ss interface is disabled.");
	}
}


void Interface::tts( const std::string& msg,
				const std::string& file,
				const std::string& language,
				const std::string& engine,
				const std::string& voice_font )
{
	if ( impl_->ss_pub_ ) {
		ros::NodeHandle n;
		std::stringstream ss;
		ss << impl_->ss_request_id_;
		SpeechSynthesisRequest request;
		request.header.language = language;
		request.header.voice_font = voice_font;
		request.header.engine = engine;
		request.header.user = n.getNamespace();
		request.header.request_id = ss.str();
		request.header.request_type = SpeechSynthesisHeader::REQUEST_TYPE_TTS;
		request.message = msg;
		impl_->ss_pub_.publish(request);
		impl_->ss_request_id_++;

	} else {
		ROS_INFO("ss interface is disabled.");
	}
}


void Interface::recognize( const std::string& data,
						const std::string& language,
						const std::string& engine )
{
	if ( impl_->sr_pub_ ) {
		ros::NodeHandle n;
		std::stringstream ss;
		ss << impl_->sr_request_id_;
		SpeechRecognitionRequest request;
		request.header.language = language;
		request.header.engine = engine;
		request.header.user = n.getNamespace();
		request.header.request_id = ss.str();
		request.data = data;
		impl_->sr_pub_.publish(request);
		impl_->sr_request_id_++;

	} else {
		ROS_INFO("sr interface is disabled.");
	}
}


void Interface::registerSRResponse( const SpeechRecognizeCallback& callback )
{
	impl_->sr_func_ = callback;
}


void Interface::registerSRResponse( void(*fp)(const std::string& msg) )
{
	impl_->sr_func_ = SpeechRecognizeCallback(fp);
}


void Interface::registerSSResponse( const SpeechSynthesisCallback& callback )
{
	impl_->ss_func_ = callback;
}


void Interface::registerSSResponse( void(*fp)(const std::string& data) )
{
	impl_->ss_func_ = SpeechSynthesisCallback(fp);
}


void Interface::setSPIConfig( const std::string& language, const std::string& engine)
{
	impl_->spi_language_ = language;
	impl_->spi_engine_ = engine;

	ros::service::waitForService(SPI_CONFIG_SERVICE_NAME, -1);
	ros::NodeHandle nh;
	ros::ServiceClient client = nh.serviceClient<SpeechRecognitionConfig>(SPI_CONFIG_SERVICE_NAME);
	SpeechRecognitionConfig srv;
	srv.request.engine = engine;
	srv.request.language = language;
	if ( client.call(srv) ) {
		ROS_DEBUG("Success to call service %s", SPI_CONFIG_SERVICE_NAME);

	} else {
		ROS_ERROR("Failed to call service %s", SPI_CONFIG_SERVICE_NAME);
	}
}


void Interface::playSound( const std::string& file_name )
{
	impl_->playSound(file_name);
}
