^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package rospeex_if
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.14.7 (2016-03-30)
-------------------

2.14.6 (2016-01-14)
-------------------
* Add Speech Synthesis Cient input message limit
* Contributors: Takamasa Sasagawa

2.14.5 (2016-01-14)
-------------------

2.14.4 (2015-12-01)
-------------------

2.14.3 (2015-12-01)
-------------------

2.14.2 (2015-11-26)
-------------------
* Modified docomo API and Microsoft API to rospeex_core tests
* Add docomo API and Microsoft API to rospeex_core

2.14.1 (2015-09-18)
-------------------

2.14.0 (2015-09-18)
-------------------
* Add ros topic publisher argument 'queue_size'

2.13.0 (2015-09-14)
-------------------
* Fixed rospeex_if
* Fixed rospeex_ss/ss_state topic

2.12.6 (2015-04-24)
-------------------

2.12.5 (2015-03-03)
-------------------

2.12.4 (2015-02-19)
-------------------
* Merge branch 'release/2.12.4'
* change application from ffplay to aplay.

2.12.3 (2015-02-13)
-------------------
* fix issue #69

2.12.2 (2015-01-07)
-------------------
* fix issue #68
* update version rospeex_if/setup.py

2.12.1 (2014-12-26)
-------------------
* fix test file
* add test data, modify test script.
* fix test file
* fix test script
* add test script permission and test data
* fix setupscript and package.xml
* add test scripts
* fix package.xml
* fix #62
* fix source comment, and split source code.
* fix comment
* modify package.xml
* modify lanch files, and rospeex_web audiomonitor
* update sample script / update cmake permissions
* fix install script
* fix filenames / add audiomonitor
* fix launch files
* change rospeex/msgs directory
* add rospeex_sample
* create rospeex_if / rospeex_core
