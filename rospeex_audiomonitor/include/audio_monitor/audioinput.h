/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QPixmap>

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <QtWidgets>
#else
#include <QWidget>
#include <QMainWindow>
#endif

#include <QObject>
#include <QPushButton>
#include <QComboBox>
#include <qaudioinput.h>
#include <QFile>
#include <QQueue>

#include "soundserver.h"
#include "AtrasrClient.h"
extern ATRASR_CLIENT _atrasrClient;
extern int _epdPort;

class AudioInfo : public QIODevice
{
    Q_OBJECT
public:
    AudioInfo(QObject* parent, QAudioInput* device, SoundServer *server);
    ~AudioInfo();

    void start();
    void stop();

    class Level
    {
    public:
        Level(int _min = 0, int _max = 0, bool _isRecording = false, bool _isEpdOn = true)
            : min(_min/32768.0), max(_max/32768.0), recording(_isRecording), epdOn(_isEpdOn)
        {

        }

        float min;
        float max;
        bool recording;
        bool epdOn;
    };
    QList<Level> getLevels();

    qint64 readData(char *data, qint64 maxlen);
    qint64 writeData(const char *data, qint64 len);

    bool epdEnabled() const;
    void setEpdEnabled(bool bEnabled);

    static void setFileName(const QString& filename);

private:
    void processWave(const void *data, qint64 len);
    QAudioInput*   input;

public slots:
    void recstart();
    void recstop(bool bEnd = true);

private slots:
    void epdRead();
    void onCmd(const QString &cmd);
    void onClientConnected(const QHostAddress &addr);
    void epdDisconnected();

private:
    bool m_bRec;
    bool m_bEpdOn;
    bool m_bEpdEnabled;
    QList<Level> m_levels;
    QFile *m_file;
    int m_sampleSize;
    static QString m_filename;
    SoundServer *m_server;

signals:
    void update();
    void validateRecButton(bool);
};


class RenderArea : public QWidget
{
    Q_OBJECT

public:
    RenderArea(QWidget *parent = 0);

    void setLevel(const QList<AudioInfo::Level> &levels);

protected:
    void paintEvent(QPaintEvent *event);

private:
    QQueue<AudioInfo::Level> m_levels;

    static int STEP;
};

class SoundServerApp : public QMainWindow
{
    Q_OBJECT
public:
    SoundServerApp(SoundServer *server);
    ~SoundServerApp();

private:
    SoundServer *server;

    QAudioDeviceInfo device;
    QAudioFormat   format;
    QAudioInput*   audioInput;
    AudioInfo*     audioinfo;
    QIODevice*     input;
    RenderArea*    canvas;

    bool           pullMode;

    QPushButton*   recButton;
    QPushButton*   button;
    QPushButton*   button2;
    QComboBox*     deviceBox;

    char*          buffer;

private slots:
    void refreshDisplay();
    void status();
    void readMore();
    void toggleMode();
    void toggleSuspend();
    void state(QAudio::State s);
    void deviceChanged(int idx);
    void epdEnableChanged(bool);
    void epdSpeechChanged(bool);

    void recstart();
};

