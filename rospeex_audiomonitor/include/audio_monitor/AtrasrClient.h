
#ifndef ATRASRCLIENT_H
#define ATRASRCLIENT_H

#include <QTcpSocket>

// Frame size.
#define ATRASR_FRAME_HEADER_SIZE 4
#define ATRASR_FRAME_DATA_SIZE   320 /* 160 * 2 ( 10ms of 16kHz 16bit sampling ) */
#define ATRASR_FRAME_SIZE (ATRASR_FRAME_HEADER_SIZE + ATRASR_FRAME_DATA_SIZE)

#define ATRASR_SEND_BUF_FRAME_COUNT 100 // 1 sec
#define ATRASR_SEND_BUF_SIZE (ATRASR_FRAME_SIZE * ATRASR_SEND_BUF_FRAME_COUNT)

#define ATRASR_RECV_BUF_FRAME_COUNT 100 // 1 sec
#define ATRASR_RECV_BUF_SIZE (ATRASR_FRAME_SIZE * ATRASR_RECV_BUF_FRAME_COUNT)

/**
 * Frame type.
 */
typedef enum
{
    ATRASR_TOF   = 9,
    ATRASR_START = 1,
    ATRASR_DATA  = 0,
    ATRASR_END   = 2,
    ATRASR_CANCEL= 6, // treated as END if received.
    ATRASR_EOF   = 10

} ATRASR_FRAME_TYPE;

/**
 * Client object.
 */
typedef struct
{
    QTcpSocket *socket;

    /** Buffer for send. */
    unsigned char sendBuf[ATRASR_SEND_BUF_SIZE + ATRASR_FRAME_SIZE];
    int curIndexSendBuf;
    int sendFrameCount;

    /** Buffer for receive. */
    unsigned char recvBuf[ATRASR_RECV_BUF_SIZE];
    int curIndexRecvBuf;
    int recvFrameCount;
    
    char *logFilename;

} ATRASR_CLIENT;

/**
 * Connect to ATRASR.
 * @return 0 to success.
 * @param client  the client object.
 * @param srvHost host name.
 * @param srvPort port number.
 */
int AtrasrConnect( ATRASR_CLIENT *client, const char *srvHost, int srvPort );

/**
 * Disconnect to ATRASR.
 * @param client  the client object.
 */
void AtrasrClose( ATRASR_CLIENT *client );

/**
 * Send TOF(Top of file).
 * @return 0 to success.
 * @param client  the client object.
 */
int AtrasrSendTOF( ATRASR_CLIENT *client );

/**
 * Send EOF(End of file).
 * @return 0 to success.
 * @param client  the client object.
 */
int AtrasrSendEOF( ATRASR_CLIENT *client );

/**
 * Send sound data.
 * @return 0 to success.
 * @param client  the client object.
 * @param data  sound data buffer.
 * @param size  length of the data.
 */
int AtrasrSendDATA( ATRASR_CLIENT *client, const char *data, size_t size );

/**
 * Send STARTPU (start of speech).
 * @return 0 to success.
 * @param client  the client object.
 */
int AtrasrSendSTARTPU( ATRASR_CLIENT *client );

/**
 * Send ENDPU (end of speech).
 * @return 0 to success.
 * @param client  the client object.
 */
int AtrasrSendENDPU( ATRASR_CLIENT *client );

/**
 * Send CANCELPU (cancel of speech).
 * @return 0 to success.
 * @param client  the client object.
 */
int AtrasrSendCANCELPU( ATRASR_CLIENT *client );

/**
 * Send a file(raw or wav) to ATRASR.
 * @param client  the client object.
 * @param fileName  name of file.
 */
void AtrasrSendDataFromFile( ATRASR_CLIENT *client, const char *fileName );

/**
 * Send a raw data( 16kHz, LittleEndian ) to ATRASR.
 * @param client  the client object.
 * @param data  sound data buffer.
 * @param size  length of the data.
 */
void AtrasrSendRawData( ATRASR_CLIENT *client, const char *data, size_t size );

/**
 * Receive frames.
 * @return  received frame count.
 * @param client  the client object.
 */
int AtrasrReceiveFrame( ATRASR_CLIENT *client );

/**
 * Parse received frames.
 * @return  the pointer to sound data.
 * @param client  the client object.
 * @param nIndex  the index of the received frames.
 * @param type  [out] Frame type.
 */
unsigned char *AtrasrParseFrame( ATRASR_CLIENT *client, int nIndex, long *type );

#endif /* #ifndef ATRASRCLIENT_H */
