#include "serversocket.h"
#include <QDebug>

ServerSocket::ServerSocket(int socketDescriptor, QObject *parent)
    : QObject(parent)
{
    m_bSendingData = false;

    if (!socket.setSocketDescriptor(socketDescriptor)) {
        qCritical() << socket.errorString();
        return;
    }

    connect(&socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(&socket, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
}

QHostAddress ServerSocket::peerAddress() const
{
    return socket.peerAddress();
}

void ServerSocket::readyRead()
{
    QList<QByteArray> cmds = socket.readAll().split('\n');
    for (int i = 0; i < cmds.count(); i++){
        QString cmd = cmds[i];
        emit read(cmd);
    }

    /*
    char buf[256];
    qint64 lineLength = socket.read(buf, sizeof(buf));
    if (lineLength == -1) {
        qCritical() << "reading from client failed.";
    }
    else if (lineLength > 0){
        buf[lineLength-1] = '\0';
        emit read(QString(buf));
    }
    */
}

void ServerSocket::clientDisconnected()
{
    qDebug() << "disconnected.";
    emit disconnected();
}

void ServerSocket::SendStart()
{
    _Send( "BEGIN\n\n" );
    m_bSendingData = true;
}

void ServerSocket::SendData( const void *data, int len )
{
    if ( m_bSendingData ){
        _Send( QString("DATA %1\n\n").arg(len) );
        _Send( data, len );
    }
}

void ServerSocket::SendEnd()
{
    if ( m_bSendingData ){
        _Send( "END\n\n" );
        m_bSendingData = false;
    }
}

void ServerSocket::SendCancel()
{
    if ( m_bSendingData ){
        _Send( "CANCEL\n\n" );
        m_bSendingData = false;
    }
}

void ServerSocket::SendKeyPressed( int val )
{
    _Send( QString("KEY_PRESSED %1\n\n").arg(val) );
}

void ServerSocket::SendEpdOn()
{
    _Send( "EPD_ON\n\n" );
}

void ServerSocket::SendEpdOff()
{
    _Send( "EPD_OFF\n\n" );
}

void ServerSocket::_Send( const QString& str )
{
    _Send( str.toAscii(), str.length() );
}

void ServerSocket::_Send( const void *data, int len )
{
    socket.write((const char*)data, len);
}
