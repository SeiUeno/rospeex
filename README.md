Introduction
============
Rospeex is a cloud-based multilingual communication toolkit for ROS. Rospeex provides an easy-to-use interface for speech recognition and speech synthesis in English, Chinese, Japanese, and Korean. Switching to third-party's cloud engines is also possible with rospeex. Official website: http://rospeex.org/

Licensing
=========
Please check following website.
http://rospeex.org/license-en/
