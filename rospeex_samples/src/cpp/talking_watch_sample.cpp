#include <iostream>
#include <sstream>
#include <boost/regex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <ros/ros.h>
#include "rospeex_if/rospeex.h"

static rospeex::Interface interface;

void sr_response( const std::string& msg )
{
	using boost::posix_time::ptime;
	using boost::posix_time::second_clock;

	std::cerr << "you said : " << msg << std::endl;
	boost::regex time_reg(".*何時.*");
	std::string text = "";
	if ( boost::regex_match(msg, time_reg) ) {
		ptime now = second_clock::local_time();
		std::stringstream ss;
		ss << now.time_of_day().hours() << "時" << now.time_of_day().minutes() << "分です";
		text = ss.str();
		std::cerr << "robot reply : " << text << std::endl;
		interface.say( text, "ja", "nict" );
	}
}

int main( int argc, char** argv )
{
	ros::init(argc, argv, "sr_ss_demo");

	interface.init();
	interface.registerSRResponse( sr_response );
	interface.setSPIConfig("ja", "nict");
	ros::spin();
	return 0;
}
