#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import python libraries
import os
import rospy
import logging
import unittest
import ConfigParser

# import pypi libraries
from nose.tools import raises
from nose.tools import nottest

# import local libraries
from rospeex_core.ss.docomo_aitalk import Client
from rospeex_core import exceptions as ext

# setup logging
FORMAT = '[%(levelname)s] %(message)s @%(filename)s:%(funcName)s:%(lineno)d'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestVoiceTextClient(unittest.TestCase):
    def setUp(self):
        base_dir = os.path.dirname(__file__)
        settings = ConfigParser.ConfigParser()
        filename = os.path.join(base_dir, 'config.cfg')

        settings.read(filename)

        self.big_text_file = os.path.join(
            base_dir,
            settings.get('SpeechSynthesis', 'big_text_for_docomo_aitalk_file')
        )
        self.api_key = settings.get('SpeechSynthesis', 'docomo_api_key')

        self.big_text_data = None
        with open(self.big_text_file, 'r') as f:
            self.big_text_data = f.read()

    @raises(ext.ParameterException)
    def test_request_invalid_message(self):
        message = None
        language = 'ja'
        voice_font = '*'
        timeout = 100
        client = Client()
        client._key = self.api_key
        client.request(message, language, voice_font, timeout)

    @raises(ext.ParameterException)
    def test_request_invalid_too_long_message(self):
        message = ''
        language = 'ja'
        voice_font = 'seiji'
        timeout = 100
        client = Client()
        client._key = self.api_key

        # create too long message
        for i in range(300):
            message = message + 'a'

        client.request(message, language, voice_font, timeout)

    @raises(ext.ParameterException)
    def test_request_invalid_long_utf8_message(self):
        message = u''
        language = 'ja'
        voice_font = 'seiji'
        timeout = 100
        client = Client()
        client._key = self.api_key

        # create too long message
        for i in range(201):
            message = message + u'あ'
        client.request(message, language, voice_font, timeout)

    @nottest
    @raises(ext.InvalidResponseException)
    def test_request_invalid_message_charactor(self):
        message = '<'
        language = 'ja'
        voice_font = 'seiji'
        timeout = 100
        client = Client()
        client._key = self.api_key
        client.request(message, language, voice_font, timeout)

    @raises(ext.UnsupportedLanguageException)
    def test_request_invalid_language(self):
        message = 'hello'
        language = 'de'        # deutsch
        voice_font = 'seiji'
        timeout = 100
        client = Client()
        client._key = self.api_key
        client.request(message, language, voice_font, timeout)

    @raises(ext.ParameterException)
    def test_request_invalid_api_key(self):
        message = 'hello'
        language = 'ja'        # deutsch
        voice_font = 'seiji'
        timeout = 100
        client = Client()
        client.request(message, language, voice_font, timeout)

    @raises(ext.InvalidResponseException)
    def test_request_invalid_url(self):
        message = 'hello'
        language = 'ja'
        voice_font = 'seiji'
        timeout = 100
        client = Client()
        client.URL = 'http://rospeex.ucri.jgn-x.jp/nauth_json/jsServices/hoge'
        client._key = self.api_key
        client.request(message, language, voice_font, timeout)

    @raises(ext.ParameterException)
    def test_request_invalid_voice_font(self):
        message = 'hello'
        language = 'ja'
        voice_font = 'hogehoge'
        timeout = 100
        client = Client()
        client._key = self.api_key
        client.request(message, language, voice_font, timeout)

    @nottest
    @raises(ext.RequestTimeoutException)
    def test_request_server_timeout_post(self):
        message = self.big_text_data
        language = 'ja'
        voice_font = 'seiji'
        timeout = 2
        client = Client()
        client._key = self.api_key
        client.request(message, language, voice_font, timeout)

    @nottest
    def test_request_valid_big_message(self):
        message = self.big_text_data
        language = 'ja'
        voice_font = 'seiji'
        timeout = 1000000
        client = Client()
        client._key = self.api_key
        client.request(message, language, voice_font, timeout)

    @nottest
    def test_request_valid_japanese_message(self):
        message = u'こんにちは'
        language = 'ja'
        voice_font = 'seiji'
        timeout = 10000
        client = Client()
        client._key = self.api_key
        client.request(message.encode('utf-8'), language, voice_font, timeout)

    @nottest
    def test_request_valid_voice_fonts(self):
        message = u'こんにちは'
        language = 'ja'
        timeout = 10000
        client = Client()
        client._key = self.api_key

        for speaker in client.SPEAKER_LIST:
            client.request(message.encode('utf-8'), language, speaker, timeout)
            rospy.sleep(2)


if __name__ == '__main__':
    import rosunit
    test_class = TestVoiceTextClient
    rosunit.unitrun(
        'rospeex_core',
        'speech_synthesis_client_docomo_aitalk',
        test_class,
        None,
        coverage_packages=['rospeex_core.ss']
    )
