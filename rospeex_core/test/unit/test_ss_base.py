#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import python libraries
import logging
import unittest

from nose.tools import raises
from rospeex_core.ss.base import IClient


# setup logging
FORMAT = '[%(levelname)s] %(message)s @%(filename)s:%(funcName)s:%(lineno)d'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestSpeechSynthesisClient(unittest.TestCase):
    def setUp(self):
        pass

    @raises(TypeError)
    def test_request_invalid_not_implement(self):
        client = IClient()
        client.request('')


if __name__ == '__main__':
    import rosunit
    test_class = TestSpeechSynthesisClient
    rosunit.unitrun(
        'rospeex_core',
        'speech_synthesis_client_base',
        test_class,
        None,
        coverage_packages=['rospeex_core.ss']
    )
