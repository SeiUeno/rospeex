#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging
import unittest
import ConfigParser

from nose.tools import raises
from rospeex_core.sr.google import Client
from rospeex_core import exceptions as ext


# setup logging
FORMAT = '[%(levelname)s] %(message)s @%(filename)s:%(funcName)s:%(lineno)d'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestSpeechRecognitionClient_Google(unittest.TestCase):
    def setUp(self):
        base_dir = os.path.dirname(__file__)
        filename = os.path.join(base_dir, 'config.cfg')
        settings = ConfigParser.ConfigParser()
        settings.read(filename)
        self.flac_file = os.path.join(
            base_dir,
            settings.get('SpeechRecognition', 'flac_file')
        )
        self.broken_wav_file = os.path.join(
            base_dir,
            settings.get('SpeechRecognition', 'broken_wav_file')
        )
        self.wav_file = os.path.join(
            base_dir,
            settings.get('SpeechRecognition', 'wav_file')
        )
        self.access_key = settings.get(
            'SpeechRecognition',
            'google_access_key'
        )

    @raises(ext.ParameterException)
    def test_request_invalid_audio_format_flac(self):
        request_data = open(self.flac_file, 'rb').read()
        client = Client()
        client._key = self.access_key
        client.request(request_data)

    @raises(ext.ParameterException)
    def test_request_invalid_audio_format_broken_wav(self):
        language = 'ja'
        request_data = open(self.broken_wav_file, 'rb').read()
        client = Client()
        client._key = self.access_key
        client.request(data=request_data, language=language)

    @raises(ext.ParameterException)
    def test_request_invalid_language(self):
        language = 'hoge'
        request_data = open(self.wav_file, 'rb').read()
        client = Client()
        client._key = self.access_key
        client.request(data=request_data, language=language)

    @raises(ext.ParameterException)
    def test_request_invalid_access_key(self):
        language = 'ja'
        request_data = open(self.wav_file, 'rb').read()
        client = Client()
        client._key = None
        client.request(data=request_data, language=language)

    @raises(ext.RequestTimeoutException)
    def test_request_invalid_request_timeout(self):
        language = 'ja'
        request_data = open(self.wav_file, 'rb').read()
        client = Client()
        client._key = self.access_key
        client.request(data=request_data, language=language, timeout=1)

    def test_request_valid_language(self):
        for language in Client.LANGUAGES:
            request_data = open(self.wav_file, 'rb').read()
            client = Client()
            client._key = self.access_key
            msg = client.request(data=request_data, language=language)
            logger.info(msg)


if __name__ == '__main__':
    import rosunit
    test_class = TestSpeechRecognitionClient_Google
    rosunit.unitrun(
        'rospeex_core',
        'speech_recognition_client_google',
        test_class,
        None,
        coverage_packages=['rospeex_core.sr']
    )
