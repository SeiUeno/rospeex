#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import python libraries
import logging
import unittest

# import pypi libraries
from nose.tools import raises

# import ros libraries
from rospeex_core.ss import Factory
from rospeex_core import exceptions as ext

# setup logging
FORMAT = '[%(levelname)s] %(message)s @%(filename)s:%(funcName)s:%(lineno)d'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestSpeechSynthesisFactory(unittest.TestCase):
    def setUp(self):
        pass

    @raises(ext.SpeechSynthesisException)
    def test_create_invelid_engine(self):
        Factory.create('hoge')

    def test_create_valid_engine(self):
        for factory in Factory.ENGINE_FACTORY.keys():
            Factory.create(factory)


if __name__ == '__main__':
    import rosunit
    test_class = TestSpeechSynthesisFactory
    rosunit.unitrun(
        'rospeex_core',
        'speech_synthesis_factory',
        test_class,
        None,
        coverage_packages=['rospeex.ss']
    )
