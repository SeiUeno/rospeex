#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import python libraries
import logging
import unittest

# import pypi libraries
from nose.tools import raises

# import local libraries
from rospeex_core import exceptions as ext
from rospeex_core.sr.base.client import IClient
from rospeex_core.sr.base.session import SessionState

# setup logging
FORMAT = '[%(levelname)s] %(message)s @%(filename)s:%(funcName)s:%(lineno)d'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestSpeechRecognitionClient(unittest.TestCase):
    def setUp(self):
        pass

    @raises(TypeError)
    def test_request_invalid_not_implement(self):
        IClient()


class TestSessionState(unittest.TestCase):
    def setUp(self):
        pass

    def test_to_str(self):
        assert SessionState.to_str(SessionState.ERROR) == 'ERROR'
        assert SessionState.to_str(SessionState.INIT) == 'INIT'
        assert SessionState.to_str(SessionState.START) == 'START'
        assert SessionState.to_str(SessionState.DATA) == 'DATA'
        assert SessionState.to_str(SessionState.END) == 'END'
        assert SessionState.to_str(-100) == 'UNKNOWN STATE'

    def test_check_valid_state(self):
        SessionState.check_state(SessionState.ERROR)
        SessionState.check_state(SessionState.INIT)
        SessionState.check_state(SessionState.START)
        SessionState.check_state(SessionState.DATA)
        SessionState.check_state(SessionState.END)

    @raises(ext.InvalidSessionStateException)
    def test_check_invalid_state(self):
        SessionState.check_state(-100)


if __name__ == '__main__':
    import rosunit
    rosunit.unitrun(
        'rospeex_core',
        'sr_base_client',
        TestSpeechRecognitionClient,
        None,
        coverage_packages=['rospeex_core']
    )

    rosunit.unitrun(
        'rospeex_core',
        'sr_base_session_state',
        TestSessionState,
        None,
        coverage_packages=['rospeex_core']
    )
