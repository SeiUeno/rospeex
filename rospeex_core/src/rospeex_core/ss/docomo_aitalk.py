# -*- coding: utf-8 -*-

import rospy
import traceback
import requests

import wave
import struct
import StringIO

# import library
from rospeex_core import logging_util
from rospeex_core.validators import accepts
from rospeex_core.validators import check_language
from rospeex_core import exceptions as ext
from rospeex_core.ss.base import IClient
from rospeex_core.ss import nict


logger = logging_util.get_logger(__name__)


class Client(IClient):
    """ SpeechSynthesisCient_docomo_aitalk class """
    LANGUAGES = ['ja']

    # docomo_aitalk parameters
    VERSION = "v1"
    URL = "https://api.apigw.smt.docomo.ne.jp/aiTalk/%s/textToSpeech" % VERSION

    SPEAKER_LIST = ['nozomi','seiji','akari','anzu','hiroshi','kaho','koutarou','maki','nanako','osamu','sumire', '*']
    TEXT_RANGE = [0, 200]

    DEFAULT_VOICE_FONT = 'nozomi'

    def __init__(self):
        """ init function
        @param args: parameter list for initialize
        @type args: list
        @param kwargs: parameter dictionary for initialize
        """
        self._key = ''
        self._speakerid  = 0
        self._speechrate = 1.0
        self._powerrate  = 1.0
        self._voicetype  = 0
        self._audiofileformat  = 0
        self._load_parameter()

    def _load_parameter(self):
        """ load parameter from rospy """
        try:
            self._key = rospy.get_param('~docomo_api_key', '')
        except Exception:
            pass

    @accepts(message=basestring, language=str, voice_font=str, timeout=int)
    def request(self, message, language='ja', voice_font='nozomi', timeout=10):
        """
        Send speech synthesis request to server,
        and get speech synthesis result.
        @param message: message
        @type  message: str
        @param language: speech synthesis language
        @type  language: str
        @param voice_font: taraget voice font
        @type  voice_font: str
        @param timeout: request timeout time (second)
        @type  timeout: float
        @return: voice data (wav format binary)
        @rtype: str
        @raise SpeechSynthesisException:
        """
        # check language
        check_language(language, self.LANGUAGES)

        # check api key
        self._check_api_key()

        # check speaker
        self._check_voice_font(voice_font)
        if voice_font == '*':
            voice_font = self.DEFAULT_VOICE_FONT

        # check text
        self._check_text(message)

        # send data
        try:
            client = nict.Client()
            client.request(message, language, '*', 10)
        except Exception:
            pass

        # create ssml request
        template = u"<?xml version='1.0' encoding='utf-8' ?>"\
            "<speak version='1.1'><voice name='{font}'>{message}</voice></speak>"

        self._ssml_text = template.format(
            font=voice_font,
            message=message.decode('utf-8')
        )

        # create parameters
        headers = {
            'Content-Type': 'application/ssml+xml',
            'Accept': 'audio/L16'
        }

        params = {
            'APIKEY': self._key,
        }

        try:
            response = requests.post(
                self.URL,
                headers=headers,
                data=self._ssml_text.encode('utf-8'),
                params=params,
                timeout=timeout,
            )

        except requests.exceptions.Timeout as err:
            msg = 'request time out. Exception: {err}'.format(
                err=str(err)
            )
            raise ext.RequestTimeoutException(msg)

        except requests.exceptions.ConnectionError as err:
            msg = 'network connection error. Exception: {err}'.format(
                err=str(err)
            )
            raise ext.InvalidRequestException(msg)

        except requests.exceptions.RequestException as err:
            msg = 'invalid request error. Exception: {err}'.format(
                err=str(err)
            )
            raise ext.InvalidRequestException(msg)

        # check response
        if response.status_code == 200:
            wav_buffer = self._create_wav_buffer(response.content)
            return wav_buffer

        else:
            self._check_errors(response.status_code)

    def _check_api_key(self):
        """ check api key
        @raises ParameterException
        """
        if self._key is None or self._key == '':
            msg = 'parameter failed. if you want to use docomo_aitalk engine'\
                   'you MUST set api key for docomo_aitalk api.'
            raise ext.ParameterException(msg)

    def _check_text(self, text):
        """ check tts text
        @param text: text
        @type str
        @raises ParameterException
        """
        if not len(text) in range(*self.TEXT_RANGE):
            msg = 'parameter failed. text length is not in range.'\
                  'Except: {range[0]} <= text_length:{value}'\
                  ' <= {range[1]}'.format(
                        range=self.TEXT_RANGE,
                        value=len(text)
                    )
            raise ext.ParameterException(msg)

    def _check_voice_font(self, voice_font):
        """ check voice font
        @param voice_font: voice font
        @type voice_font: str
        @raises ParameterException
        """
        if voice_font not in self.SPEAKER_LIST:
            msg = 'parameter failed [{voice_font}].'\
                  'you choose voice fonts following parametrs '\
                  '{speaker_list}'.format(
                        voice_font=voice_font,
                        speaker_list=str(self.SPEAKER_LIST)
                    )
            raise ext.ParameterException(msg)

    def _check_errors(self, status_code):
        """ check server errors
        @param status_code: status code
        @type  status_code: int
        @raises InvalidRequestException
        @raises InvalidResponseException
        """

        if status_code == 400:
            msg = 'invalid request. StatusCode: {status_code}'.format(
                status_code=status_code
            )
            raise ext.InvalidRequestException(msg)

        elif status_code == 401:
            msg = 'auth failed. StatusCode: {status_code}'.format(
                status_code=status_code
            )
            raise ext.InvalidRequestException(msg)

        elif status_code == 404:
            msg = 'target url is not avariable. StatusCode: {status_code}'.format(
                status_code=status_code
            )
            raise ext.InvalidRequestException(msg)

        elif status_code == 405:
            msg = 'invalid request. StatusCode: {status_code}'.format(
                status_code=status_code
            )
            raise ext.InvalidRequestException(msg)

        elif status_code == 500:
            msg = 'server internal error. StatusCode: {status_code}'.format(
                status_code=status_code
            )
            raise ext.InvalidResponseException(msg)

        elif status_code == 503:
            msg = 'service unavailable error. StatusCode: {status_code}'.format(
                status_code=status_code
            )
            raise ext.InvalidResponseException(msg)

        else:
            msg = 'undefined error. StatusCode: {status_code} '\
                  'Traceback:{trace}'.format(
                        status_code=status_code,
                        trace=traceback.format_exc()
                    )
            raise ext.InvalidResponseException(msg)

    def _create_wav_buffer(self, content):
        """ create wave buffer
        @param content: binary data
        @type  content: str
        @raises WaveConvertException
        """

        if content is None or content == '':
            msg = 'wave convert failed. input binary data is null.'
            raise ext.WaveConvertException(msg)

        try:
            buffer = StringIO.StringIO()
            
            w = wave.open(buffer, 'w')

            buf_len = len(content) / 2 
            fmt = '<%dH' % buf_len
            unpacked_buf = struct.unpack(fmt, content)
            
            fmt = '>%dH' % buf_len
            packed_buf = struct.pack(fmt, *unpacked_buf)

            param = (1, 2, 16000, len(content), 'NONE', 'not compressed')
            w.setparams(param)
            w.writeframes(packed_buf)
            buffer.flush()

            if buffer is None or buffer == '':
                msg = 'wave convert failed. converted binary data is null.'
                raise ext.WaveConvertException(msg)

            wav_buffer = buffer.getvalue()

            w.close()
            buffer.close()

        except Exception as err:
            msg = 'wave convert failed.{err}'.format(
                err=str(err)
            )
            raise ext.WaveConvertException(msg)
            
        return wav_buffer
