#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Import ROS packages
import rospy

# Import python libraries
import subprocess

# Import Messages
from rospeex_msgs.msg import SpeechSynthesisRequest
from rospeex_msgs.msg import SpeechSynthesisResponse
from rospeex_msgs.msg import SpeechSynthesisHeader
from rospeex_msgs.msg import SpeechSynthesisState

# Import speech synthesis client
from rospeex_core import exceptions as ext
from rospeex_core.ss import Factory


class SpeechSynthesis(object):
    """ SpeechSynthesis class """
    MY_NODE_NAME = 'rospeex_ss'
    REQUEST_TOPIC_NAME = 'ss_req'
    RESPONSE_TOPIC_NAME = 'ss_res'
    STATE_TOPIC_NAME = 'ss_state'

    def __init__(self):
        """ init function """
        self._pub_res = None
        self._pub_state = None
        self._user_request_dict = {}

    def _update_user_dictionary(self, header):
        """ renew user request dictionary
        @param header: request header
        @type  header: str
        """
        if header.user in self._user_request_dict:
            self._user_request_dict[header.user] += 1
            if int(header.request_id) != self._user_request_dict[header.user]:
                # check request id
                msg = 'invalid request id. user:{user} '\
                      'current id:{curr_id} previous id:{prev_id}'.format(
                        user=header.user,
                        curr_id=int(header.request_id),
                        prev_id=self._user_request_dict[header.user]
                        )
                rospy.logwarn(msg)
            # update request id
            self._user_request_dict[header.user] = int(header.request_id)

        else:
            self._user_request_dict.update(
                {header.user: int(header.request_id)}
            )
            rospy.logdebug('add new user:%s' % header.user)

    def _play_data(self, response):
        """ play audio data
        @param response: audio data
        @type  response: str
        """
        try:
            # play state = True
            self._publish_state(response.header, True)

            # write sound data
            tmp_file = open('output_tmp.dat', 'wb')
            tmp_file.write(response.data)
            tmp_file.close()

            # play sound
            args = ['aplay', '-q', 'output_tmp.dat']
            subprocess.check_output(args, stderr=subprocess.STDOUT)

        except IOError:
            rospy.logerr('aplay io error.')

        except OSError:
            rospy.logerr('aplay is not installed.')

        except subprocess.CalledProcessError:
            rospy.logerr('aplay return error value.')

        except:
            rospy.logerr('unknown exception.')

        finally:
            self._publish_state(response.header, False)

    @classmethod
    def _save_data(cls, file_name, data):
        """ save data to target file
        @param file_name: target file
        @type  file_name: str
        @param data: audio data
        @type  data: str
        """
        with open(file_name, 'wb') as tmp_file:
            tmp_file.write(data)

    def _publish_state(self, header, state):
        """ publish state topic
        @param header: speech synthesis header
        @type  header: str
        @param state: audio state
        @type  state: bool
        """
        ss_state = SpeechSynthesisState()
        ss_state.header = header
        ss_state.play_state = state
        self._pub_state.publish(ss_state)
        rospy.loginfo(ss_state)

    def _callback_func(self, request):
        """ callback functioin for ss_req topic
        @param request: request data
        @param request: str
        """
        rospy.loginfo(request)
        response = SpeechSynthesisResponse()
        response.header = request.header
        response.data = ''

        # update user dictionary
        self._update_user_dictionary(request.header)

        try:
            client = Factory.create(request.header.engine)
            response.data = client.request(
                request.message,
                request.header.language,
                request.header.voice_font
            )

            request_type = request.header.request_type

            # if command is "say" play speech synthesis sound
            if request_type == SpeechSynthesisHeader.REQUEST_TYPE_SAY:
                self._play_data(response)

            # if commsnd is "tts" save data
            elif request_type == SpeechSynthesisHeader.REQUEST_TYPE_TTS:
                self._save_data(request.memo, response.data)

        except ext.SpeechSynthesisException as err:
            rospy.logerr('[SpeechSynthesisExcepton] ' + str(err))
            response.memo = str(err)

        except ext.ParameterException as err:
            rospy.logerr('[ParameterException] ' + str(err))
            response.memo = str(err)

        except ext.ServerException as err:
            rospy.logerr('[ServerException] ' + str(err))
            response.memo = str(err)

        # send speech synthesis response to subscribe node
        rospy.loginfo(response)
        self._pub_res.publish(response)

    def run(self):
        """ node main function """
        rospy.init_node(self.MY_NODE_NAME)

        rospy.Subscriber(
            self.REQUEST_TOPIC_NAME,
            SpeechSynthesisRequest,
            self._callback_func
        )

        self._pub_res = rospy.Publisher(
            self.RESPONSE_TOPIC_NAME,
            SpeechSynthesisResponse,
            queue_size=10
        )

        self._pub_state = rospy.Publisher(
            self.STATE_TOPIC_NAME,
            SpeechSynthesisState,
            queue_size=10
        )

        rospy.loginfo("start speech synthesis node")
        rospy.spin()


def main():
    try:
        speech_synthesis_node = SpeechSynthesis()
        speech_synthesis_node.run()

    except rospy.ROSInterruptException:
        pass
