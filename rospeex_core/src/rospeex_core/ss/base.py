# -*- coding: utf-8 -*-

import socket
import abc


__all__ = ['IClient']


class IClient(object):
    """
    IClient class
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def request(
        self,
        message,
        language='ja',
        voice_font='',
        timeout=socket._GLOBAL_DEFAULT_TIMEOUT
    ):
        """
        Send speech synthesis request to server,
        and get speech synthesis result.
        @param message: message
        @type  message: str
        @param language: speech synthesis language
        @type  language: str
        @param voice_font: taraget voice font
        @type  voice_font: str
        @param timeout: request timeout time (second)
        @type  timeout: float
        @raise SpeechSynthesisException:
        """
        pass
