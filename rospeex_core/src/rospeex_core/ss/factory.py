# -*- coding: utf-8 -*-


# Import local libraries
from rospeex_core import exceptions as ext
from rospeex_core.ss.nict import Client as NICTClient
from rospeex_core.ss.google import Client as GoogleClient
from rospeex_core.ss.docomo_hoya import Client as docomo_hoyaClient
from rospeex_core.ss.docomo_aitalk import Client as docomo_aitalkClient
# from rospeex_core.ss.voicetext import Client as VoiceTextClient


class SSEngine(object):
    """ engine enum class """
    NICT = 'nict'
    GOOGLE = 'google'
    # VOICETEXT = 'voicetext'
    DOCOMO_HOYA = 'docomo_hoya'
    DOCOMO_AITALK = 'docomo_aitalk'


class SpeechSynthesisFactory(object):
    """ SpeechSynthesisFactory class """
    ENGINE_FACTORY = {
        'google': GoogleClient,
        'nict': NICTClient,
        # 'voicetext': VoiceTextClient,
        'docomo_hoya': docomo_hoyaClient,
        'docomo_aitalk': docomo_aitalkClient,
    }

    @classmethod
    def create(cls, engine=''):
        """
        create speech synthesis engine.
        @param engine: speech synthesis engine (nict / google / voicetext / docomo_hoya / docomo_aitalk)
        @type  engine: str
        @raise SpeechSynthesisException:
        """
        if engine in cls.ENGINE_FACTORY.keys():
            return cls.ENGINE_FACTORY[engine]()
        else:
            msg = 'target client [{engine}] is not found. '\
                  'Except: {engine_list}'.format(
                        engine=engine,
                        engine_list=cls.ENGINE_FACTORY.keys()
                    )
            raise ext.SpeechSynthesisException(msg)
