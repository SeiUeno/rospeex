# -*- coding: utf-8 -*-

# import library
from rospeex_core import exceptions as ext
from rospeex_core import logging_util
from rospeex_core.sr import nict
from rospeex_core.sr import google
from rospeex_core.sr import microsoft

# create logger
logger = logging_util.get_logger(__name__)


class SREngine(object):
    """ Engine class """
    NICT = 'nict'
    GOOGLE = 'google'
    MICROSOFT = 'microsoft'


class SpeechRecognitionFactory(object):
    """
    SpeechRecognitionFactory class
    """
    ENGINE_FACTORY = {
        'google': google.Client,
        'nict': nict.Client,
        'microsoft': microsoft.Client,
    }

    @classmethod
    def create(cls, engine, *args, **kwargs):
        """
        create speech recognition client
        @param engine: speech recognition engine name (NICT|GOOGLE|MICROSOFT)
        @type  engine: str
        @param args:
        @param kwargs:
        @raise SpeechRecognitionException:
        """
        if engine in cls.ENGINE_FACTORY.keys():
            return cls.ENGINE_FACTORY[engine](*args, **kwargs)

        else:
            msg = 'target client [{engine}] is not found.'\
                  'Except: {engine_list}'.format(
                        engine=engine,
                        engine_list=cls.ENGINE_FACTORY.keys()
                    )
            raise ext.SpeechRecognitionException(msg)
