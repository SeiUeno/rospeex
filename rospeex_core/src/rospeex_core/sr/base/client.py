# -*- coding: utf-8 -*-

# import python libraries
import abc
import socket


__all__ = ['IClient']


class IClient(object):
    """ IClient class """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def request(
        self,
        data,
        language='ja',
        timeout=socket._GLOBAL_DEFAULT_TIMEOUT
    ):
        """
        send speech recognition request to server
        @param data: speech binary data
        @type  data: str
        @param language: speech data language
        @type  language: str
        @param timeout: time out time[ms]
        @type  timeout: int
        """
        pass

    @abc.abstractmethod
    def support_streaming(self):
        """
        check support streaming
        @returns: True for support streaming / False for NOT support streaming
        """
        pass

    @abc.abstractmethod
    def set_streaming_config(self, *args, **kwargs):
        """
        set streaming config
        @param argv:
        @param kwargs:
        """
        pass

    @abc.abstractmethod
    def add_streaming_packet(self, packet_type, packet_data):
        """
        add streaming packet
        @param packet_type:
        @type  packet_type: int
        @param packet_data:
        @param packet_data: str
        """
        pass

    @abc.abstractmethod
    def register_streaming_cb(self, cb):
        """
        register streaming result callback
        @param cb:
        @type cb:
        """
        pass

    @abc.abstractmethod
    def unregister_streaming_cb(self, cb):
        """
        unregister streaming result callback
        @param cb:
        @type cb:
        """
        pass

    @abc.abstractmethod
    def join(self, timeout):
        """
        join streaming client
        @param timeout:
        @type timeout:
        """
        pass
