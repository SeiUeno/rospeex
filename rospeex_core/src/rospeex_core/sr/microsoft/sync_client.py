#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import python libraries
import urllib
import urllib2
import json
import socket
import traceback
import ssl

import uuid
import requests
import platform

import rospy

# import local libraries
import rospeex_core.exceptions as ext
from rospeex_core import logging_util
from rospeex_core.validators import accepts, check_wave_data, check_language
from rospeex_core.sr.base.client import IClient
from rospeex_core.sr.nict import Client

# create logger
logger = logging_util.get_logger(__name__)


class SyncClient(IClient):
    """ SpeechRecognitionClient_Microsoft class """
    AUDIO_LENGTH = 16000
    FRAMERATE = 16000
    CHANNELS = 1
    SAMPWIDTH = 2
    LANGUAGES = ['en', 'ko', 'ja', 'zh']
    MICROSOFT_LANGUAGES = {
        'en': 'en-US',
        'ko': 'ko-KR',
        'ja': 'ja-JP',
        'zh': 'zh-CN'
    }
    DEFAULT_LANGUAGE = 'ja-JP'
    URL = 'https://speech.platform.bing.com'
    USER_AGENT = 'rospeex.MicrosoftSynClient'
    UNIQUE_ID = str(uuid.uuid4()).replace('-', '')

    def __init__(self, microsoft_client_id=None, microsoft_client_secret=None, *args, **kwargs):
        """ initialize function """
        self._client_id = microsoft_client_id
        self._client_secret = microsoft_client_secret
        self._instance_id = self._generate_id()
        self._token = ''

    def _authorize(self):
        """ send web authorization request to server """
        url = 'https://oxford-speech.cloudapp.net//token/issueToken'

        headers = {
            'Content-type': 'application/x-www-form-urlencoded'
        }

        params = {
            'grant_type': 'client_credentials',
            'client_id': self._client_id,
            'client_secret': self._client_secret,
            'scope': self.URL
        }

        self._check_client_id()
        self._check_client_secret()

        response = requests.post(url, data=params, headers=headers)
        if response.ok:
            _body = response.json()
            self._token = _body['access_token']
        else:
            response.raise_for_status()

    @accepts(data=str, languate=str, timeout=int)
    def request(
        self,
        data,
        language='ja',
        timeout=socket._GLOBAL_DEFAULT_TIMEOUT
    ):
        """ send speech recognition request to server
        @param data: speech binary data
        @type  data: str
        @param language: speech data language
        @type  language: str
        @param timeout: time out time[ms]
        @type  timeout: int
        """
        self._check_client_id()
        self._check_client_secret()

        check_wave_data(
            data,
            self.FRAMERATE,
            self.CHANNELS,
            self.SAMPWIDTH,
            self.AUDIO_LENGTH
        )
        check_language(language, self.LANGUAGES)

        lang = self.DEFAULT_LANGUAGE

        if language in self.MICROSOFT_LANGUAGES:
            lang = self.MICROSOFT_LANGUAGES[language]
        else:
            rospy.logwarn(
               '%s is not supported. set locale default to [%s].',
               language, self.DEFAULT_LANGUAGE
            )
            lang = self.DEFAULT_LANGUAGE
            
        # speech recognition by nict engine
        nict_result = ''
        try:
            client = Client()
            nict_result = client.request(data, language, 10)

        except Exception:
            pass

        self._authorize()

        # speech recognition by microsoft engine
        result_text = None
        try:
            result_text = self._request_microsoft_server(
                self._client_id,
                self._client_secret,
                lang,
                data,
                timeout
            )

        except ext.InvalidResponseException:
            logger.info(
                'microsoft speech api connection failed. thus use nict api.'
            )
            result_text = nict_result

        return result_text

    def _check_client_id(self):
        """ check api key """
        if not self._client_id:
            msg = 'argment failed. if you want to use microsoft engine,'\
                  'you MUST set client id for microsoft speech api.'
            raise ext.ParameterException(msg)

    def _check_client_secret(self):
        """ check api key """
        if not self._client_secret:
            msg = 'argment failed. if you want to use microsoft engine,'\
                  'you MUST set client secret for microsoft speech api.'
            raise ext.ParameterException(msg)

    def _request_microsoft_server(self, client_id, client_secret, language, data, timeout):
        """ speech recognition request to microsoft server (use speech api)
        @param client_id: microsoft primary key
        @type  client_id: str
        @param client_secret: microsoft secondary key
        @type  client_secret: str
        @param language: speech data language
        @type  language: str
        @param data: speech binary data
        @type  data: str
        @param timeout: timeout time [s]
        @type  timeout: int
        @raise SpeechRecognitionException:
        """
        try:
            # リクエストを作成し、microsoftにデータを送信する
            req = self._create_request(client_id, client_secret, language, data)
            res = urllib2.urlopen(req, timeout=timeout)
            res_read = res.read()
            microsoft_result_text = self._process_data(res_read)

        except urllib2.URLError as err:
            if isinstance(err.reason, socket.timeout):
                raise ext.RequestTimeoutException(
                    'request time out. Exception: %s',
                    str(err)
                )
            raise ext.InvalidRequestException(
                'request url error. Exception:%s',
                str(err)
            )

        except urllib2.HTTPError as err:
            raise ext.InvalidResponseException(
                'http error. %s Exception:%s',
                err.code,
                err.msg
            )

        except (ssl.SSLError, socket.timeout) as err:
            raise ext.RequestTimeoutException(str(err))

        except Exception as err:
            msg = 'unknown exception. Traceback: {}'.format(
                traceback.format_exc()
            )
            raise ext.SpeechRecognitionException(msg)

        return microsoft_result_text

    def _create_request(self, client_id, client_secret, language, data):
        """ create http request data for microsoft speech api
        @param client_id: microsoft primary api key
        @type  client_id: str
        @param client_secret: microsoft secondary key
        @type  client_secret: str
        @param language: speech data language
        @type  language: str
        @param data: speech binary data
        @type  data: str
        """
        samplerate = 8000
        scenarios = 'ulm'

        params = {
            'version': '3.0',
            'appID': 'D4D52672-91D7-4C74-8AD8-42B1D98141A5',
            'instanceid': self._instance_id,
            'requestid': self._generate_id(),
            'format': 'json',
            'locale': language,
            'device.os': platform.system() + ' ' + platform.release(),
            'scenarios': scenarios,
        }

        headers = {
            'Content-type': 'audio/wav; samplerate={0}'.format(samplerate),
            'Authorization': 'Bearer ' + self._token,
            'X-Search-AppId': self.UNIQUE_ID,
            'X-Search-ClientID': self._instance_id,
            'User-Agent': self.USER_AGENT
        }

        url_req = self.URL + '/recognize/query?' + urllib.urlencode(params)
        request = urllib2.Request(url_req, data, headers)
        return request

    def _process_data(self, input_str):
        result_list = input_str.split('\n')
        json_result_list = []
        for result in result_list:
            try:
                json_result_list.append(json.loads(result))
            except:
                pass

        # データの抽出
        result_data = self._extract_result_key_data(json_result_list)
        if result_data != '':
            result_data = self._extract_lexical_data(result_data)

        # get data
        result_text = result_data[0] if len(result_data) else ''
        return result_text

    @classmethod
    def _extract_result_key_data(cls, input_data):
        """ extract result data from server response
        @param input_data:
        @type  input_data: dict()
        @returns: extract result data from serer response
        @rtype: str or unicode
        """
        # 必要なデータを取り出す
        result_data = [
            result['results'] for result in input_data if 'results' in result
        ]
        if len(result_data) is 0:
            raise ext.InvalidResponseException(
                'result key is not found. Input: %s',
                input_data
            )

        result_data = filter(lambda x: len(x), result_data)
        if len(result_data) is 0:
            return ''

        result_data = reduce(lambda a, b: a+b, result_data)
        return result_data

    @classmethod
    def _extract_lexical_data(cls, input_data):
        """ extract lexical data from server response
        @param input_data:
        @type  input_data: dict()
        @returns: extract lexical data from server response as result data
        @rtype: str or unicode
        """
        # result['lexical'] を持つデータを取得する
        result_data = [
            result['lexical'] for result in input_data if 'lexical' in result
        ]
        if len(result_data) is 0:
            raise ext.InvalidResponseException(
                'lexical key is not found. Input: %s',
                input_data
            )
        return result_data

    def support_streaming(self):
        """
        check support streaming
        @returns: True for support streaming / False for NOT support streaming
        """
        return False

    def add_streaming_packet(self, packet_type, packet_data):
        """
        add streaming packet
        @param packet_type:
        @type  packet_type: int
        @param packet_data:
        @type  packet_data: str
        """
        pass

    def register_streaming_cb(self, cb):
        """
        register streaming result callback
        @param cb:
        @type cb:
        """
        pass

    def unregister_streaming_cb(self, cb):
        """
        unregister streaming result callback
        @param cb:
        @type cb:
        """
        pass

    def set_streaming_config(self, language):
        """
        set streaming config
        @param language:
        @type  language: str
        """
        pass

    def join(self, timeout=None):
        """
        join streaming client
        @param timeout:
        @type  timeout:
        """
        pass

    @classmethod
    def _generate_id(cls):
        return str(uuid.uuid4()).replace('-', '')


