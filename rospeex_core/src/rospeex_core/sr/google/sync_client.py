#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import python libraries
import urllib
import urllib2
import json
import socket
import traceback
import ssl

# import local libraries
import rospeex_core.exceptions as ext
from rospeex_core import logging_util
from rospeex_core.validators import accepts, check_wave_data, check_language
from rospeex_core.sr.base.client import IClient
from rospeex_core.sr.nict import Client

# create logger
logger = logging_util.get_logger(__name__)


class SyncClient(IClient):
    """ SpeechRecognitionClient_Google class """
    AUDIO_LENGTH = 16000
    FRAMERATE = 16000
    CHANNELS = 1
    SAMPWIDTH = 2
    LANGUAGES = ['ja', 'en']
    URL = 'https://www.google.com/speech-api/v2/recognize?'

    def __init__(self, google_api_key=None, *args, **kwargs):
        """ initialize function """
        self._api_key = google_api_key

    @accepts(data=str, languate=str, timeout=int)
    def request(
        self,
        data,
        language='ja',
        timeout=socket._GLOBAL_DEFAULT_TIMEOUT
    ):
        """ send speech recognition request to server
        @param data: speech binary data
        @type  data: str
        @param language: speech data language
        @type  language: str
        @param timeout: time out time[ms]
        @type  timeout: int
        """
        check_wave_data(
            data,
            self.FRAMERATE,
            self.CHANNELS,
            self.SAMPWIDTH,
            self.AUDIO_LENGTH
        )
        check_language(language, self.LANGUAGES)
        self._check_api_key()

        # speech recognition by nict engine
        nict_result = ''
        try:
            client = Client()
            nict_result = client.request(data, language, 10)

        except Exception:
            pass

        # speech recognition by google engine
        result_text = None
        try:
            result_text = self._request_google_server(
                self._api_key,
                language,
                data,
                timeout
            )

        except ext.InvalidResponseException:
            logger.info(
                'google speech api connection failed. thus use nict api.'
            )
            result_text = nict_result

        return result_text

    def _check_api_key(self):
        """ check api key """
        if not self._api_key:
            msg = 'argment failed. if you want to use google engine,'\
                  'you MUST set api key for google speech api v2.'
            raise ext.ParameterException(msg)

    def _request_google_server(self, access_key, language, data, timeout):
        """ speech recognition request to google server (use speech api v2)
        @param access_key: google speech api key
        @type  access_key: str
        @param language: speech data language
        @type  language: str
        @param data: speech binary data
        @type  data: str
        @param timeout: timeout time [s]
        @type  timeout: int
        @raise SpeechRecognitionException:
        """
        try:
            # リクエストを作成し、googleにデータを送信する
            req = self._create_request(access_key, language, data)
            res = urllib2.urlopen(req, timeout=timeout)
            res_read = res.read()
            google_result_text = self._process_data(res_read)

        except urllib2.URLError as err:
            if isinstance(err.reason, socket.timeout):
                raise ext.RequestTimeoutException(
                    'request time out. Exception: %s',
                    str(err)
                )
            raise ext.InvalidRequestException(
                'request url error. Exception:%s',
                str(err)
            )

        except urllib2.HTTPError as err:
            raise ext.InvalidResponseException(
                'http error. %s Exception:%s',
                err.code,
                err.msg
            )

        except (ssl.SSLError, socket.timeout) as err:
            raise ext.RequestTimeoutException(str(err))

        except Exception as err:
            msg = 'unknown exception. Traceback: {}'.format(
                traceback.format_exc()
            )
            raise ext.SpeechRecognitionException(msg)

        return google_result_text

    def _create_request(self, access_key, language, data):
        """ create http request data for google speech api v2
        @param access_key: google speech api key
        @type  access_key: str
        @param language: speech data language
        @type  language: str
        @param data: speech binary data
        @type  data: str
        """
        header = {'Content-Type': 'audio/l16; rate=16000;'}
        values = {
            'output': 'json',
            'lang': language,
            'key': access_key
        }
        url_req = self.URL + urllib.urlencode(values)
        request = urllib2.Request(url_req, data, header)
        return request

    def _process_data(self, input_str):
        result_list = input_str.split('\n')
        json_result_list = []
        for result in result_list:
            try:
                json_result_list.append(json.loads(result))
            except:
                pass

        # データの抽出
        result_data = self._extract_result_key_data(json_result_list)
        if result_data != '':
            result_data = self._extract_alternative_final_key_data(result_data)
            result_data = self._extract_final_data(result_data)
            result_data = self._extract_transcript_data(result_data)

        # get data
        result_text = result_data[0] if len(result_data) else ''
        return result_text

    @classmethod
    def _extract_result_key_data(cls, input_data):
        """ extract result data from server response
        @param input_data:
        @type  input_data: dict()
        """
        # 必要なデータを取り出す
        result_data = [
            result['result'] for result in input_data if 'result' in result
        ]
        if len(result_data) is 0:
            raise ext.InvalidResponseException(
                'result key is not found. Input: %s',
                input_data
            )

        result_data = filter(lambda x: len(x), result_data)
        if len(result_data) is 0:
            return ''

        result_data = reduce(lambda a, b: a+b, result_data)
        return result_data

    @classmethod
    def _extract_alternative_final_key_data(cls, input_data):
        """ extract alternative key data
        @param input_data:
        @type  input_data: dict()
        """
        # key=>alternative と key=>final を持つ結果を取得する
        result_data = filter(
            lambda x: 'alternative' in x and 'final' in x, input_data
        )
        if len(result_data) is 0:
            raise ext.InvalidResponseException(
                'alternative key is not found. Input: %s',
                input_data
            )
        return result_data

    @classmethod
    def _extract_final_data(cls, intput_data):
        """ extract final data from server response
        @param input_data:
        @type  input_data: dict()
        """
        # result['final'] is True のデータを取得する
        result_data = [
            result['alternative'] for result in intput_data
            if len(result['alternative']) > 0 and result['final'] is True
        ]
        if len(result_data) is 0:
            raise ext.InvalidResponseException(
                'final key is not found. Input: %s',
                intput_data
            )
        return result_data

    @classmethod
    def _extract_transcript_data(cls, input_data):
        """ extract transcript data from server response
        @param input_data:
        @type  input_data: dict()
        """
        # result['transcript'] を持つデータを取得する
        result_data = reduce(lambda a, b: a+b, input_data)
        result_data = [
            result['transcript'] for result in result_data
            if 'transcript' in result
        ]
        if len(result_data) is 0:
            raise ext.InvalidResponseException(
                'transcript key is not found. Input: %s',
                input_data
            )
        return result_data

    def support_streaming(self):
        """
        check support streaming
        @returns: True for support streaming / False for NOT support streaming
        """
        return False

    def add_streaming_packet(self, packet_type, packet_data):
        """
        add streaming packet
        @param packet_type:
        @type  packet_type: int
        @param packet_data:
        @param packet_data: str
        """
        pass

    def register_streaming_cb(self, cb):
        """
        register streaming result callback
        @param cb:
        @type cb:
        """
        pass

    def unregister_streaming_cb(self, cb):
        """
        unregister streaming result callback
        @param cb:
        @type cb:
        """
        pass

    def set_streaming_config(self, language):
        """ set streaming config
        @param language:
        """
        pass

    def join(self, timeout=None):
        """
        join streaming client
        @param timeout:
        @type timeout:
        """
        pass
