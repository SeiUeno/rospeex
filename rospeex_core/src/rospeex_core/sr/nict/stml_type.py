#!-*- coding: utf-8 -*-

# python libraries
import abc
import xml.etree.ElementTree as ET


class STMLElement(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_element(self):
        pass    # pragma: no cover


class VoiceFormat(STMLElement):
    """ VoiceFormat class
    """
    def __init__(self):
        self._root = ET.Element('Voice')

    def add_id(self, id):
        self._root.set('ID', id)

    def get_element(self):
        return self._root


class UserFormat(STMLElement):
    """ User format class
    """
    def __init__(self):
        self._root = ET.Element('User')

    def add_id(self, user_id):
        self._root.set('ID', user_id)

    def get_element(self):
        return self._root


class InputAudioFormat(STMLElement):
    """ input audio format class
    """
    def __init__(self):
        self._root = ET.Element('InputAudioFormat')

    def add_endian(self, endian):
        self._root.set('Endian', endian)

    def add_audio(self, audio):
        self._root.set('Audio', audio)

    def get_element(self):
        return self._root


class OutputTextFormat(STMLElement):
    """ output audio format class
    """
    def __init__(self):
        self._root = ET.Element('OutputTextFormat')

    def add_form(self, form):
        self._root.set('Form', form)

    def get_element(self):
        return self._root


class SRInputType(STMLElement):
    """ speech recognition input type
    """
    def __init__(self):
        self._root = ET.Element('SR_IN')

    def add_language(self, language):
        self._root.set('Language', language)

    def add_domain(self, domain):
        self._root.set('Domain', domain)

    def add_max_nbest(self, max_nbest):
        self._root.set('MaxNBest', str(max_nbest))

    def add_output_text_format(self, output_format):
        self._root.append(output_format.get_element())

    def add_input_audio_format(self, input_format):
        self._root.append(input_format.get_element())

    def add_voice(self, voice):
        self._root.append(voice.get_element())

    def get_element(self):
        return self._root


class STMLType(object):
    """ stml builder class """
    def __init__(self):
        self._root = ET.Element('STML')
        self._root.set(
            'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance'
        )

    def add_version(self, version):
        self._root.set('Version', str(version))

    def add_utterance_id(self, utterance_id):
        self._root.set('UtteranceID', utterance_id)

    def add_user(self, user_id):
        self._root.append(user_id.get_element())

    def add_sr_in(self, sr_in):
        self._root.append(sr_in.get_element())

    def tostring(self):
        return ET.tostring(self._root, encoding="UTF-8", method="xml")
