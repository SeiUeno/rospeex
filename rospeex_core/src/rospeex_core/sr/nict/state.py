# -*- coding: utf-8 -*-

# python libraries
from email.parser import Parser
from xml.etree import ElementTree as ET

# pypi libraries
import requests

# local libraries
from rospeex_core import logging_util
from rospeex_core.sr.base import IState
from rospeex_core.sr.base import PacketType
from rospeex_core.sr.base import SessionState
from rospeex_core.sr.base import IRequest
from mime_message import MIMEMessage


# get logger
logger = logging_util.get_logger(__name__)


class NICTRequest(IRequest):
    """ NICT Request class
    """
    def __init__(self, url, data=None, cookies=None):
        """ init class

        @param url:
        @param data:
        @param cookies:
        """
        self._url = url
        self._cookies = cookies
        self._data = data
        self._response = None
        self._has_err = False
        self._stml_text = ''

    def _create_request_string(self):
        pass    # pragma: no cover

    def request(self, *args, **kwargs):
        """ request to server

        @param args: not used
        @param kwargs: not used
        """
        # create message
        headers, message = self._create_request_string()
        try:
            self._response = requests.post(
                self._url,
                data=message,
                headers=headers,
                timeout=10.0
            )

        except requests.exceptions.RequestException as err:
            self._has_err = True
            msg = str(err)
            logger.warn(msg)
            return

        # check response
        if self._response.status_code == requests.codes.ok:
            try:
                self._stml_text = self._parse_mime_text(self._response.text)
                self._check_stml_format(self._stml_text)

            except Exception as err:
                self._has_err = True
                msg = 'Invalid MIME format. Err {}'.format(err)
                logger.exception(msg)

        else:
            self._has_err = True
            msg = 'server [{}] returns error code [{}]'.format(
                self._url,
                self._response.status_code
            )
            logger.warn(msg)

    def _parse_mime_text(self, mime_text):
        """ parse mime data format

        @param mime_text:
        @return: stml text
        """
        parser = Parser()
        encoded_text = mime_text.encode('utf-8')
        message = parser.parsestr(encoded_text)
        response_text = None
        for part in message.walk():
            if part.get_content_type() == 'text/xml':
                response_text = part.get_payload()
                break
        return response_text

    def _check_stml_format(self, stml_text):
        root = ET.fromstring(stml_text)
        for elem in root.findall('.//Error'):
            self._has_err = True
            msg = 'stml string has error attr. [{}]'.format(elem.text)
            logger.warn(msg)

    def has_error(self):
        """ execute error check

        @reutrn: True for Error occured / False for Succes.
        """
        return self._has_err

    def response(self):
        """ get response
        @return: response data
        """
        return self._response

    def result(self):
        return self._stml_text


class StartRequest(NICTRequest):
    """ StartRequest class
    """
    def __init__(self, url, language, cookies=None):
        """ init class

        @param url:
        @param cookies:
        """
        super(StartRequest, self).__init__(url, None, cookies)
        self._language = language

    def _create_request_string(self):
        """ create request string
        """
        headers = MIMEMessage.create_http_header(self._cookies)
        message = MIMEMessage.create_header_request(self._language, 1)
        return headers, message


class DataRequest(NICTRequest):
    def __init__(self, url, data=None, cookies=None):
        """ init class

        @param url:
        @param data:
        @param cookies:
        """
        super(DataRequest, self).__init__(url, data, cookies)

    def _create_request_string(self):
        """ create request string
        """
        headers = MIMEMessage.create_http_header(self._cookies)
        message = MIMEMessage.create_data_request(self._data)
        return headers, message


class EndRequest(NICTRequest):
    def __init__(self, url, cookies=None):
        """ init class

        @param url:
        @param cookies:
        """
        super(EndRequest, self).__init__(url, None, cookies)

    def _create_request_string(self):
        """ create request string
        """
        headers = MIMEMessage.create_http_header(self._cookies)
        message = MIMEMessage.create_finish_request()
        return headers, message


class NICTClientState(IState):
    def run(self, packet_data):
        pass    # pragma: no cover

    def next(self, packet_type):
        pass    # pragma: no cover

    def result(self):
        pass    # pragma: no cover

    def state(self):
        pass    # pragma: no cover

    def _create_url(self, base_url, index, is_last):
        if is_last:
            url = '{base}/n{index}last'.format(
                base=base_url,
                index=index
            )
        else:
            url = '{base}/n{index}'.format(
                base=base_url,
                index=index
            )
        return url


class InitState(NICTClientState):
    def __init__(self, base_url, language, pool):
        self._pool = pool
        self._language = language
        self._base_url = base_url
        self._request = None

    def run(self, packet_data):
        pass    # pragma: no cover

    def next(self, packet_type):
        if packet_type == PacketType.START:
            logger.debug('Change session state INIT -> START')
            return StartState(
                self._base_url,
                1,
                self._language,
                None,
                self._pool
            )

        else:
            logger.debug('Change session state INIT -> ERROR')
            return ErrorState(
                self._base_url,
                0,
                self._language,
                None,
                self._pool
            )

    def result(self):
        return None     # pragma: no cover

    def state(self):
        return SessionState.INIT


class StartState(NICTClientState):
    def __init__(self, base_url, index, language, cookie, pool):
        self._pool = pool
        self._language = language
        self._base_url = base_url
        self._index = index
        self._request = None
        self._cookie = cookie

    def run(self, packet_data):
        url = self._create_url(self._base_url, self._index, False)
        self._request = StartRequest(url, self._language, self._cookie)
        self._request.request()
        response = self._request.response()
        if response:
            self._cookie = response.headers['set-cookie']

    def next(self, packet_type):
        if self._request.has_error():
            logger.debug('Change session state START -> ERROR')
            return ErrorState(
                self._base_url,
                0,
                self._language,
                self._cookie,
                self._pool
            )

        if packet_type == PacketType.DATA:
            logger.debug('Change session state START -> DATA')
            return DataState(
                self._base_url,
                self._index+1,
                self._language,
                self._cookie,
                self._pool
            )

        elif packet_type == PacketType.END:
            logger.debug('Change session state START -> END')
            return EndState(
                self._base_url,
                self._index+1,
                self._language,
                self._cookie,
                self._pool
            )

        else:
            logger.debug('Change session state START -> ERROR')
            return ErrorState(
                self._base_url,
                0,
                self._language,
                self._cookie,
                self._pool
            )

    def result(self):
        return None     # pragma: no cover

    def state(self):
        return SessionState.START


class DataState(NICTClientState):
    def __init__(self, base_url, index, language, cookie, pool):
        self._pool = pool
        self._language = language
        self._base_url = base_url
        self._index = index
        self._request = None
        self._cookie = cookie

    def run(self, packet_data):
        url = self._create_url(self._base_url, self._index, False)
        self._request = DataRequest(url, packet_data, self._cookie)
        self._pool.add_request(self._request)

    def next(self, packet_type):
        if packet_type == PacketType.DATA:
            logger.debug('Change session state DATA -> DATA')
            return DataState(
                self._base_url,
                self._index+1,
                self._language,
                self._cookie,
                self._pool
            )

        elif packet_type == PacketType.END:
            logger.debug('Change session state DATA -> END')
            task_state = self._check_task_state()
            if task_state:
                return EndState(
                    self._base_url,
                    self._index+1,
                    self._language,
                    self._cookie,
                    self._pool
                )

            else:
                logger.debug('Change session state DATA -> ERROR')
                return ErrorState(
                    self._base_url,
                    0,
                    self._language,
                    self._cookie,
                    self._pool
                )

        elif packet_type == PacketType.START:
            task_state = self._check_task_state()
            return StartState(
                self._base_url,
                1,
                self._language,
                None,
                self._pool
            )

        else:
            logger.debug('Change session state DATA -> ERROR')
            return ErrorState(
                self._base_url,
                0,
                self._language,
                self._cookie,
                self._pool
            )

    def _check_task_state(self):
        self._pool.wait_completion()
        success = True
        while True:
            task = self._pool.get_finish_task(wait=False)
            if task:
                if task.has_error():
                    success = False
            else:
                break
        return success

    def result(self):
        return None     # pragma: no cover

    def state(self):
        return SessionState.DATA


class EndState(NICTClientState):
    def __init__(self, base_url, index, language, cookie, pool):
        self._pool = pool
        self._language = language
        self._base_url = base_url
        self._index = index
        self._request = None
        self._cookie = cookie
        self._result_text = ''

    def run(self, packet_data):
        url = self._create_url(self._base_url, self._index, True)
        self._request = EndRequest(url, self._cookie)
        self._request.request()

        if not self._request.has_error():
            stml_text = self._request.result()
            root = ET.fromstring(stml_text)
            elem = root.find('.//s')
            if elem is not None:
                self._result_text = elem.text

        if self._result_text is None:
            self._result_text = ''

    def next(self, packet_type):
        if self._request.has_error():
            logger.debug('Change session state END -> ERROR')
            return ErrorState(
                self._base_url,
                0,
                self._language,
                self._cookie,
                self._pool
            )

        if packet_type == PacketType.START:
            logger.debug('Change session state END -> START')
            return StartState(
                self._base_url,
                1,
                self._language,
                self._cookie,
                self._pool
            )

        else:
            logger.debug('Change session state END -> ERROR')
            return ErrorState(
                self._base_url,
                0,
                self._language,
                self._cookie,
                self._pool
            )

    def result(self):
        return self._result_text

    def state(self):
        return SessionState.END


class ErrorState(NICTClientState):
    def __init__(self, base_url, index, language, cookie, pool):
        self._pool = pool
        self._language = language
        self._base_url = base_url
        self._index = index
        self._request = None
        self._cookie = cookie

    def run(self, packet_data):
        pass

    def next(self, packet_type):
        if packet_type == PacketType.START:
            logger.debug('Change session state ERROR -> START')
            return StartState(
                self._base_url,
                1,
                self._language,
                None,
                self._pool
            )

        else:
            logger.debug('Change session state ERROR -> ERROR')
            return ErrorState(
                self._base_url,
                0,
                self._language,
                self._cookie,
                self._pool
            )

    def result(self):
        return None     # pragma: no cover

    def state(self):
        return SessionState.ERROR
