#!/usr/bin/env python
# -*- coding: utf-8 -*-


class RospeexException(Exception):
    """
    rospeex exception class

    this exception class is base of rospeex exception classes.
    """
    pass


class ParameterException(RospeexException):
    """
    Parameter exception class

    this exception is used to notify parameter error.
    like None, parameter type, etc.
    """
    pass


class UnsupportedLanguageException(ParameterException):
    """
    Unsupported Language Exception

    this exception is used to notify language parameter error.
    """
    pass


class InvalidAudioDataException(ParameterException):
    """
    Invalid Audio Data Exception

    this exceptions is used to notify audio data parameter error.
    """
    pass


class SpeechSynthesisException(RospeexException):
    """
    Speech Synthesis Exception

    this exception occures in rospeex syntesis node.
    """
    pass


class WaveConvertException(SpeechSynthesisException):
    """
    Wave Convert Exception

    this exception occures in rospeex syntesis node.
    """
    pass


class SpeechRecognitionException(RospeexException):
    """
    Speech Recognition Exception

    this exception occures in rospeex recognition node.
    """
    pass


class STMLFormatException(SpeechRecognitionException):
    """ STML Format Exception
    """
    pass


class AsyncProtocolException(SpeechRecognitionException):
    """ Async Protocol Exception
    """
    pass


class AsyncSRException(AsyncProtocolException):
    """ Asnyc SR Exception
    """
    pass


class InvalidRequestTypeException(AsyncSRException):
    """ Invalid Request Type Exception
    """
    pass


class InvalidPacketTypeException(AsyncSRException):
    """ Invalid Packet Type Exception
    """
    pass


class InvalidSessionStateException(AsyncSRException):
    """ Invalid Packet Type Exception
    """
    pass


class SignalProcessingInterfaceException(RospeexException):
    """
    Signal Processing Interface Exception

    this exception occures in rospeex signal processing interface node.
    """
    pass


class NICTmmcvError(SignalProcessingInterfaceException, IOError):
    """
    NICTmmcv Error class
    """
    pass


class ServerException(RospeexException):
    """
    Server Exception

    this exception is occured by server request / response.
    """
    pass


class InvalidRequestException(ServerException):
    """
    Invalid Request Exception
    """
    pass


class InvalidResponseException(ServerException):
    """
    Invalid Response Exception
    """
    pass


class RequestTimeoutException(ServerException):
    """
    Server timeout exception
    """
    pass


class WebAudioMonitorException(SignalProcessingInterfaceException):
    """
    WebAudioMonitor Exception class
    """
    pass


class InvalidDataHeaderException(SignalProcessingInterfaceException):
    """
    Invalid Data Header Exception class
    """
    pass
