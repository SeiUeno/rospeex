# -*- coding: utf-8 -*-

import os
import json
import logging

# logging flag
__use_ros_logging = True


class Logger(object):
    """ logger class
    """
    info = None
    warning = None
    degbug = None
    critical = None
    exception = None


def setup_logging(
    default_dict={},
    default_path='logger_config.json',
    default_level=logging.INFO,
    use_ros_logging=False
):
    """ setup logging configuration
        default_dict / default_path の両者が設定されている場合、両方のデータを読み込む
        データの優先度としては、default_pathに記述されたものが優先度が高い

    :param default_dict: default logging config dict
    :param default_path: default logging config path
    :param default_level: default log level
    """
    global __use_ros_logging
    __use_ros_logging = use_ros_logging
    setup_dict = {}
    setup_dict.update(default_dict)

    if os.path.exists(default_path):
        with open(default_path, 'rb') as config_file:
            setup_dict = json.load(config_file)

    if len(setup_dict.keys()):
        logging.config.dictConfig(setup_dict)
    else:
        FORMAT = '[%(levelname)s] %(message)s @%(filename)s:%(funcName)s:%(lineno)d'
        logging.basicConfig(format=FORMAT, level=default_level)


def get_logger(name=''):
    """ get logger

    :param name: logger name
    :returns: logger
    """
    global __use_ros_logging
    logger = logging.getLogger(name)
    try:
        if __use_ros_logging:
            import rospy
            logger = Logger()
            logger.info = rospy.loginfo
            logger.debug = rospy.logdebug
            logger.warn = rospy.logwarn
            logger.err = rospy.logerr
            logger.critical = rospy.logfatal
            logger.exception = rospy.logfatal
    except:
        logger.info('rospy package is not available')

    return logger
