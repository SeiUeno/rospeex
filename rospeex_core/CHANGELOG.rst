^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package rospeex_core
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.14.7 (2016-03-30)
-------------------

2.14.6 (2016-01-14)
-------------------
* Merge branch 'develop'
* Add Speech Synthesis Cient input message limit
* Build Test on Jenkins for google ss-api

2.14.5 (2016-01-14)
-------------------

2.14.4 (2015-12-01)
-------------------
* Remove python-httplib2 from rospeex_core/package.xml

2.14.3 (2015-12-01)
-------------------
* Add timeout to rospeex_sr/nict request
* Changed python library from httplib2 to requests
* Add python-requests to rospeex_core/package.xml

2.14.2 (2015-11-26)
-------------------
* Add sr.base, sr.google, sr.microsoft, sr.nict packages to rospeex_core/setup.py
* Modified docomo API and Microsoft API to rospeex_core tests
* Add docomo API and Microsoft API to rospeex_core

2.14.1 (2015-09-18)
-------------------

2.14.0 (2015-09-18)
-------------------
* Add error handling to nict speech recognition module
* Changed LOG message level
* Hide NICTmmcvController message

2.13.0 (2015-09-14)
-------------------
* Update rospeex_sr client factory arguments.
* Update rospeex_core and rospeex_webaudiomonitor.
* Fixed a bug in google speech synthesis client
* Fixed rospeex_if
* Fixed rospeex_ss/ss_state topic
* Fixed rospeex_core/spi, ss structure
* Remove monitorServer.nex from CMakeLists.txt
* Remove load manifest

2.12.6 (2015-04-24)
-------------------
* Add libav to rospeex_core/package.xml
* modify audio converter process.

2.12.5 (2015-03-03)
-------------------
* update rospeex_core/cmakelists.txt

2.12.4 (2015-02-19)
-------------------
* Merge branch 'release/2.12.4'
* change application from ffplay to aplay.

2.12.3 (2015-02-13)
-------------------
* fix rospeex_core/cmakelists.txt
* fix issue #69

2.12.2 (2015-01-07)
-------------------
* fix #66
* fix #66

2.12.1 (2014-12-26)
-------------------
* comment out voice text
* update setup.py
* fix setupscript and package.xml
* fix package.xml
* fix ticket #20
* modify directory structure
* fix package.xml
* add x86 binary
* fix #62
* fix comment and exception message
* fix source comment, and split source code.
* fix vad parameter
* fix comment
* modify package.xml
* fix comment and modify file layout.
* fix speech recognition node
* modify lanch files, and rospeex_web audiomonitor
* add rospeex_launch package
* update cmake files
* update sample script / update cmake permissions
* fix filenames / add audiomonitor
* change rospeex/msgs directory
* add rospeex_sample
* create rospeex_if / rospeex_core
