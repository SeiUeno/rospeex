#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import rospkg
import SimpleHTTPServer

if __name__ == '__main__':
    rp = rospkg.RosPack()
    audiomonitor_path = rp.get_path('rospeex_webaudiomonitor')
    html_path = os.path.join(audiomonitor_path, 'www')
    os.chdir(html_path)
    SimpleHTTPServer.test()
