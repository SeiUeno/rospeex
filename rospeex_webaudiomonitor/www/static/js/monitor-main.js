/**
 * monitor controll object
 * @type {WebAudioMonitor}
 */
monitor = null;

/**
 * connecting state
 * @type {Boolean}
 */
connecting = false;


/**
 * Connecting state enum
 */
ConnectionState = {
  DISCONNECTED: 0,    // disconnect state
  CONNECTING: 1,      // connecting state
  CONNECTED: 2,       // connected state
  AUTO_RECONNECT: 3,    // auto reconnect state
};

/**
 * ConnectionMode enum
 */
ConnectionMode = {
  AUTO_RECONNECT: true,    // auto connect
  MANUAL_CONNECT: false,  // manual connect
};

/**
 * connection mode
 */
var connection_mode = ConnectionMode.MANUAL_CONNECT;

$(function(){
  $("[name='auto-reconnect-ckb']").bootstrapSwitch();
  $('input[name="auto-reconnect-ckb"]').on('switchChange.bootstrapSwitch', function(event, state) {
    $.cookie('auto-reconnect', state, {path: '/', expires: 30});

    if(state) {
      setReconnectMode(ConnectionMode.AUTO_RECONNECT);
    } else {
      setReconnectMode(ConnectionMode.MANUAL_CONNECT);
    }
  });

  // vad enable button
  $('#vad-enable-btn').click(function(){
    toggleSpeechDetection();
  });

  // view config ref
  $('#view-config-ref').click(function(){

    // check hidden state or show staete
    var is_hidden = $('#config-form').is(':hidden');

    if( is_hidden ) {
      displayConfigForm(true, true);

    } else {
      displayConfigForm(false, true);
    }

    return false;
  });

  // manual connect button
  $('#manual-connect-btn').click(function(){
    return setHostInfo();
  });
});


/**
 * display or hide config form
 * @param  {Boolean} flag
 * @param  {Boolean} animation
 */
function displayConfigForm(flag, animation)
{
  if(flag) {

    if (animation) $('#config-form').slideToggle(500);
    else $('#config-form').show();

    $('#view-config-ref').text("Hide Config");
    $('#collapse-indicator').show();
    $('#expand-indicator').hide();
    $.cookie('config-form-hidden', false, {path: '/', expires: 30});

  } else {

    if (animation) $('#config-form').slideToggle(500);
    else $('#config-form').hide();

    $('#view-config-ref').text("Display Config");
    $('#collapse-indicator').hide();
    $('#expand-indicator').show();
    $.cookie('config-form-hidden', true, {path: '/', expires: 30});
  }
};

/**
 * load connection informations form cookie
 */
function loadConnectionInfo()
{
  if (window.navigator.cookieEnabled) {
     // check cookie value
     var ip = $.cookie('hostIP');
     var port = $.cookie('hostPort');
     var config_panel = $.cookie('config-form-hidden') == "true" ? true : false;
     var hoge = $.cookie('auto-reconnect');

     var auto_reconnect = false;
     var tmp_auto_reconnect = $.cookie('auto-reconnect');
     if ( tmp_auto_reconnect == "true" ) {
        auto_reconnect = true;
     } else if ( tmp_auto_reconnect == "false" ) {
        auto_reconnect = false;
     } else {
        auto_reconnect = true;
     }

    // set ip address info
    if ( checkIP(ip) === true ) {
      $('#hostIP').val(ip);
    }

    // set port info
    if ( checkPort(port) === true ) {
      $('#hostPort').val(port);

    } else {
      // set default value 8000 if cookie data is invaild.
      $('#hostPort').val('9000');
    }

    // set connection mode
    if ( auto_reconnect == ConnectionMode.AUTO_RECONNECT ) {
      $('input[name="auto-reconnect-ckb"]').bootstrapSwitch('state', true, true);
      setReconnectMode(auto_reconnect);

    } else {
      $('input[name="auto-reconnect-ckb"]').bootstrapSwitch('state', false, true);
      setReconnectMode(auto_reconnect);
    }

    // config panel
    if ( config_panel == true ) {
      displayConfigForm(false, false);

    } else {
      displayConfigForm(true, false);
    }

  } else {
    $('#hostPort').val('8000');
    $('#hostIP').val('');
  }
};


/**
 * create audio monitor
 */
function createMonitor()
{
  // load campus
  var canvas_element = document.getElementById('level');

  // create monitor
  monitor = new AudioMonitor(canvas_element, onconnected, ondisconnected, onSSTextReceived, onSRTextReceived);
};


function onSRTextReceived(message)
{
  text = "<p class=\"right_balloon\">" + message + "</p>";
  $('#chat-panel').append(text);
  $('.chat-form').scrollTop($('#chat-panel')[0].scrollHeight);
};


function onSSTextReceived(message)
{
  text = "<p class=\"left_balloon\">" + message + "</p>";
  $('#chat-panel').append(text);
  $('.chat-form').scrollTop($('#chat-panel')[0].scrollHeight);
};


/**
 * set connect button state
 * @param {ConnectingState} btn_state button state
 * @param {ConnectingMode} mode conncting mode
 */
function setInterfaceState(btn_state, mode)
{
  if (btn_state == ConnectionState.DISCONNECTED && mode == ConnectionMode.MANUAL_CONNECT) {
    // set manual connect button
    $('#manual-connect-btn').removeClass('btn-info active');
    $('#manual-connect-btn').addClass('btn');
    $('#manual-connect-text').text('Manual Connect');

    // set input form
    $('#hostIP').removeAttr("disabled");
    $('#hostPort').removeAttr("disabled");

    // set indicator
    $('#status-panel-icon').removeClass('fa-times fa-exchange fa-spinner fa-spin');
    $('#status-panel-icon').addClass('fa-times');
    $('#status-panel').removeClass('alert-info alert-error alert-success');
    $('#status-panel').addClass('alert-error');

  } else if (btn_state == ConnectionState.DISCONNECTED && mode == ConnectionMode.AUTO_RECONNECT) {
    // set manual connect button
    $('#manual-connect-btn').removeClass('btn-info active');
    $('#manual-connect-btn').addClass('btn-info active');
    $('#manual-connect-text').text('Connecting');

    // set input form
    $('#hostIP').removeAttr("disabled");
    $('#hostPort').removeAttr("disabled");

    // set indicator
    $('#status-panel-icon').removeClass('fa-times fa-exchange fa-spinner fa-spin');
    $('#status-panel-icon').addClass('fa-spinner fa-spin');
    $('#status-panel').removeClass('alert-info alert-error alert-success');
    $('#status-panel').addClass('alert-info');

  } else if (btn_state == ConnectionState.CONNECTING) {
    // set manual connect button
    $('#manual-connect-btn').removeClass('btn-info active');
    $('#manual-connect-btn').addClass('btn-info active');
    $('#manual-connect-text').text('Connecting');

    // set input form
    if (mode == ConnectionMode.MANUAL_CONNECT) {
      $('#hostIP').attr('disabled', 'disabled');
      $('#hostPort').attr('disabled', 'disabled');
    } else {
      $('#hostIP').removeAttr("disabled");
      $('#hostPort').removeAttr("disabled");
    }

    // set indicator
    $('#status-panel-icon').removeClass('fa-times fa-exchange fa-spinner fa-spin');
    $('#status-panel-icon').addClass('fa-spinner fa-spin');
    $('#status-panel').removeClass('alert-info alert-error alert-success');
    $('#status-panel').addClass('alert-info');


  } else if (btn_state == ConnectionState.CONNECTED) {
    // set manual connect button
    $('#manual-connect-btn').removeClass('btn-info active');
    $('#manual-connect-btn').addClass('btn-info');
    $('#manual-connect-text').text('Connected');

    // set input form
    $('#hostIP').attr('disabled', 'disabled');
    $('#hostPort').attr('disabled', 'disabled');

    // set indicator
    $('#status-panel-icon').removeClass('fa-times fa-exchange fa-spinner fa-spin');
    $('#status-panel-icon').addClass('fa-exchange');
    $('#status-panel').removeClass('alert-info alert-error alert-success');
    $('#status-panel').addClass('alert-success');


  } else if (btn_state == ConnectionState.AUTO_RECONNECT) {
    // set manual connect button
    $('#manual-connect-btn').removeClass('btn-info active');
    $('#manual-connect-btn').addClass('btn-info active');
    $('#manual-connect-text').text('Connecting');

    // set input form
    $('#hostIP').removeAttr("disabled");
    $('#hostPort').removeAttr("disabled");

    // set indicator
    $('#status-panel-icon').removeClass('fa-times fa-exchange fa-spinner fa-spin');
    $('#status-panel-icon').addClass('fa-spinner fa-spin');
    $('#status-panel').removeClass('alert-info alert-error alert-success');
    $('#status-panel').addClass('alert-info');

  } else {
    // DO Nothing
  }

  // connectionmode
  if ( mode == ConnectionMode.AUTO_RECONNECT ) {
    $('#manual-connect-btn').attr('disabled', 'disabled');

  } else {
    $('#manual-connect-btn').removeAttr('disabled');
  }
};


/**
 * onconnected callback function
 */
function onconnected()
{
  connecting = false;
  setInterfaceState(ConnectionState.CONNECTED, connection_mode);
};


/**
 * ondisconnected callback function
 */
function ondisconnected()
{
  connecting = false;
  setInterfaceState(ConnectionState.DISCONNECTED, connection_mode);
};


/**
 * check port number
 * @param  {string} port host port number
 * @return {boolean} true for valid port / false for invalid port.
 */
function checkPort(port) {
  if ( port ) {
    var port_num = parseInt(port) || 0;
    if ( 0 < port_num && port_num <= 65535 ) {
      return true;
    }
  }
  return false;
};


/**
 * check host ip address
 * @param  {string} ip host ip address
 * @return {boolean} true for valid ip / false for invalid ip.
 */
function checkIP(ip) {
  if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)) {
    return true;
  }
  return false;
};


/**
 * check connect parameter
 * @param  {string} ip   host IP
 * @param  {string} port host Port
 * @return {boolean} true for valid parameter / false for invalid parameter.
 */
function checkConnectParameter(ip, port)
{
  var isSuccess = true;

  // check ip parameter
  if ( checkIP(ip) === false ) {
    isSuccess = false;
  }

  // check port parameter
  if ( checkPort(port) === false ) {
    isSuccess = false;
  }

  return isSuccess;
};


/**
 * Connect to server
 */
function connect()
{
  // get ui data
  var ip = $('#hostIP').get(0).value;
  var port = $('#hostPort').get(0).value;
  var auto_connect = $('input[name="auto-reconnect-ckb"]').bootstrapSwitch('state');

  // check ui parameters
  if ( checkConnectParameter(ip, port) == false ) return false;

  // save connection information to cookie
  $.cookie('hostIP', ip, {path: '/', expires: 30});
  $.cookie('hostPort', port, {path: '/', expires: 30});
  $.cookie('auto-reconnect', auto_connect, {path: '/', expires: 30});
  $.cookie('auto-reconnect', auto_connect, {path: '/', expires: 30});

  // connect to server
  try{
    if (monitor) {
      console.log('connect to monitor');
      monitor.connect(ip, port);

      // set button state
      connecting = true;

    } else {
      connecting = false;
    }

  } catch (e) {
    console.log(e);
    return false;
  }

  return true;
};


/**
 * disconnect from server
 */
function disconnect()
{
  if (monitor) {
    monitor.disconnect();
    connecting = false;
  }
  return true;
};


/**
 * set host informaton
 * @return {boolean} true for valid parameter / false for invalid parameter.
 */
function setHostInfo()
{
  if ( !monitor.isConnected() ) {
    if ( !connecting ) {
      // connect to server
      var ret = connect();
      if ( ret == true ) setInterfaceState(ConnectionState.CONNECTING, connection_mode);

    } else {
      // disconnect from server
      var ret = disconnect();
      if ( ret == true ) setInterfaceState(ConnectionState.DISCONNECTED, connection_mode);
    }

  } else {
      // disconnect from server
      disconnect();
  }

  return false;
};


/**
 * click speech detection button
 */
function toggleSpeechDetection()
{
  var buttonElement = $('#vad-enable-btn').get(0);
  var epdState = monitor.getEpdState();

  if (epdState) {
    buttonElement.value = 'Voice Activity Detection\r\nOFF';
    buttonElement.style.color = 'gray';
    monitor.setEpdState(false);

  } else {
    buttonElement.value = 'Voice Activity Detection\r\nON';
    buttonElement.style.color = 'blue';
    monitor.setEpdState(true);
  }
};


/**
 * Set Reconnection mode
 * @param {Boolean} reconnect_flg
 */
function setReconnectMode(reconnect_flg)
{
  disconnect();

  if (reconnect_flg) {
    // auto connect mode
    connection_mode = ConnectionMode.AUTO_RECONNECT;
    setAutoReconnect();
    setInterfaceState(ConnectionState.AUTO_RECONNECT, connection_mode);

  } else {
    // manual connect mode
    connection_mode = ConnectionMode.MANUAL_CONNECT;
    setInterfaceState(ConnectionState.DISCONNECTED, connection_mode);
  }
};


/**
 * set auto connect function
 */
function setAutoReconnect()
{
  // register callback if connection mode is AUTO_CONNECT
  if ( connection_mode == ConnectionMode.AUTO_RECONNECT ) {
    if ( !monitor.isConnected() ) {
      // connect to server
      var ret = connect();
    }
    setTimeout(setAutoReconnect, 5000);
  }
};


/**
 * onload function
 */
window.onload = function()
{
  // create monitor
  createMonitor();
  monitor.start();

  // load function
  loadConnectionInfo();
};
