var AUDIO_CNV_VALUE = 0xffff;
var NUM_OF_INTERVAL = 3;

var GRAY = 'rgb(100,100,100)';
var BLACK = 'rgb(0,0,0)';
var BLUE = 'rgb(0,0,255)';
var GREEN = 'rgb(0,204,153)';
var LIGHT_CYAN = 'rgb(128,255,255)';
var YELLOW = 'rgb(255,255,0)';

var DEFAULT_COLOR = BLACK;
var VAD_OFF_COLOR = GRAY;
var TALKING_COLOR = BLUE;

var DEFAULT_LEVEL_COLOR = LIGHT_CYAN;
var TALKING_LEVEL_COLOR = YELLOW;
var VAD_OFF_LEVEL_COLOR = LIGHT_CYAN;

var WIDTH = 480;
var HEIGHT = 128;

/**
 * AudioData struct
 * @param {FloatArray} data: audio data
 * @param {Boolean} vad_state: vad state flag
 * @param {Boolean} epd_state: epd state flag
 */
var AudioData = function(data, vad_state, epd_state)
{
  this.audio_data = data;
  this.vad_state = vad_state;
  this.epd_state = epd_state;
};


var LevelData = function()
{
  this.min_value = 0;
  this.max_value = 0;
  this.epd = false;
  this.vad = false;
};


/**
 * [AudioMonitor description]
 * @param {object} levelElement
 * @param {function} connected_cb
 * @param {function} disconnected_cb
 */
var AudioMonitor = function(levelElement, connected_cb, disconnected_cb, ss_text_cb, sr_text_cb)
{
  levelElement.width = WIDTH;
  levelElement.height = HEIGHT;
  this.canvas = levelElement;
  this.context = levelElement.getContext('2d');
  this.width = this.canvas.width;
  this.height = this.canvas.height;

  this.vadState = false;    // vad state provided by rospeex
  this.epdState = true;     // epd state provided by rospeex
  this.uiEpdState = true;  // epd state provided by user interface
  this.started = false;

  // set audio context (each browser)
  window.AudioContext = (
    window.AudioContext ||
    window.webkitAudioContext ||
    window.mozAudioContext ||
    window.msAudioContext
  );

  // create audio context
  this.audioContext = new window.AudioContext();
  this.analyser = this.audioContext.createAnalyser();
  this.processor = this.audioContext.createScriptProcessor(this.analyser.frequencyBinCount, 2, 1)
  this.mediastreamsource = null;

  // set sampling rate
  this.sampleRate = this.audioContext.sampleRate;
  this.chgRateCoefficient = this.sampleRate / 16000.0;

  console.log("canvas size " + this.width + "x" + this.height);
  console.log("sample rate:" + this.sampleRate);
  console.log("freq bin count:" + this.analyser.frequencyBinCount );
  console.log("change rate cofficient:" + this.chgRateCoefficient );
  console.log("fftsize:" + this.analyser.fftSize);

  // set callback functions
  this.connected_cb = connected_cb;
  this.disconnected_cb = disconnected_cb;

  // init level buffer
  this.levels = [];
  this.level_buffer = [];
  this.level_buffer_idx = 0;
  for ( var i=0; i<this.width*2; ++i ) {
    this.level_buffer.push(new LevelData());
  }

  // init audio buffer
  this.audio_que = []
  this.vad_datasize = Math.floor(this.analyser.frequencyBinCount / this.chgRateCoefficient);
  this.animation_time = new Date().getTime();

  window.requestAnimationFrame = (
    window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame
  );

  // set client
  this.client = new VadClient(
    this.clientConnected.bind(this),
    this.clientDisconnected.bind(this),
    this.vadStateChanged.bind(this),
    this.epdStateChanged.bind(this),
    this.vad_datasize,
    ss_text_cb,
    sr_text_cb
  );
}


AudioMonitor.prototype.getLevelBuffer = function()
{
  this.level_buffer_idx %= this.level_buffer.length;
  return this.level_buffer[this.level_buffer_idx++];
};


/**
 * check started
 * @return {Boolean}
 */
AudioMonitor.prototype.isStarted = function()
{
  return this.started;
};


/**
 * client disconnected callback
 */
AudioMonitor.prototype.clientConnected = function()
{
  this.connected_cb();
};


/**
 * client connected callback
 */
AudioMonitor.prototype.clientDisconnected = function()
{
  console.log("clientDisconnected called.");
  this.setVadState(false);
  this.disconnected_cb();
};


/**
 * start audio monitor
 */
AudioMonitor.prototype.start = function()
{
  if ( this.isStarted() ) {
    console.log('AudioMonitor is aleady started.');
    return;
  }

  window.navigator.getUserMedia = (
    navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia
  );

  // start audio media
  window.navigator.getUserMedia(
    {audio : true, video:false},
    this.run.bind(this),
    function(e) { console.log(e); }
  );

  // set start state
  this.started = true;
};


/**
 * connect to vad client
 * @param  {string} ip
 * @param  {string} port
 */
AudioMonitor.prototype.connect = function(ip, port)
{
  this.client.connect(ip, port);
};


/**
 * disconnect from vad client
 */
AudioMonitor.prototype.disconnect = function()
{
  this.client.disconnect();
};


/**
 * [isConnect description]
 * @return {Boolean}
 */
AudioMonitor.prototype.isConnected = function()
{
  return this.client.isConnected();
};


/**
 * set vad state
 * @param {Boolean} flag: true for / false for
 */
AudioMonitor.prototype.setVadState = function(flag)
{
  this.vadState = flag;
};

/**
 * vad state changed callback
 * @param {Boolean} flag: true for vad on / false for vad off
 */
AudioMonitor.prototype.vadStateChanged = function(flag)
{
  this.vadState = flag;
};


/**
 * get vad state
 * @return {Boolean} ture for vad on / false for vad off.
 */
AudioMonitor.prototype.getVadState = function()
{
  return this.vadState;
};


/**
 * set epd state
 * @param {Boolean} flag: true for / false for
 */
AudioMonitor.prototype.setEpdState = function(flag)
{
  this.uiEpdState = flag;
  this.client.ctrlEPD(flag);
};


/**
 * epd state changed callback
 * @param  {Boolean} flag: true for epd on / false for epd off
 */
AudioMonitor.prototype.epdStateChanged = function(flag)
{
  this.epdState = flag;
};

/**
 * get vad state
 * @return {Boolean} ture for vad on / false for vad off.
 */
AudioMonitor.prototype.getEpdState = function()
{
  return this.uiEpdState;
};


/**
 * [run description]
 * @param  {[type]} stream
 * @return {[type]}
 */
AudioMonitor.prototype.run = function(stream)
{
  // for Firefox
  URL.createObjectURL(stream);
  this.mediastreamsource = this.audioContext.createMediaStreamSource(stream);

  // set filters
  this.mediastreamsource.connect(this.processor);
  this.processor.connect(this.analyser);

  console.log( 'send_data length : ' + Math.floor( this.analyser.frequencyBinCount / this.chgRateCoefficient ) );
  this.processor.onaudioprocess = this.onaudioprocess.bind(this);

  console.log( "%%%%% animation start (stream) %%%%%");
  this.timeDomainData = new Uint8Array(this.analyser.frequencyBinCount);
  this.animation();
};


/**
 * Convert sampling rate
 * @param  {Float32Array} src: target data
 * @param  {Float32Array} dst: distination data
 * @param  {int} from: current sampling rate
 * @param  {int} to: target sampling rate
 * @return {Float32Array} converted data
 */
AudioMonitor.prototype.convertSamplingRate = function(src, from, to)
{
  var coff = from / to;
  var conv_data = new Float32Array( Math.floor(src.length / coff) );

  // define temp val
  var ceil_idx = 0;
  var ceil_data = 0.0;

  var floor_idx = 0;
  var floor_data = 0.0;

  // convert sampling rate
  var pos = coff;
  conv_data[0] = src[0];
  for (var idx=1, length=src.length-1; pos<length; idx++) {
    ceil_idx = Math.ceil(pos);
    ceil_data = src[ceil_idx];

    floor_idx = Math.floor(pos);
    floor_data = src[floor_idx];

    conv_data[idx] = (ceil_data-floor_data) * (pos - floor_idx) + floor_data;
    pos = pos + coff;
  }

  return conv_data;
};


AudioMonitor.prototype.convertSamplingRateAndUINT8 = function(src, from, to)
{
  var coff = from / to;
  var conv_data = null;

  if ( coff < 1.0 ) {
    conv_data = new Uint8Array( Math.floor((src.length-1) / coff) );
  } else {
    conv_data = new Uint8Array( Math.floor(src.length / coff) );
  }


  // define temp val
  var ceil_index = 0;
  var ceil_data = 0.0;

  var floor_index = 0;
  var floor_data = 0.0;

  var tmp_value = 0.0;

  // convert sampling rate
  var pos = coff;
  conv_data[0] = Math.round((src[0] + 1.0) / 2.0 * 255.0);
  for (var idx=1, length=src.length-1; pos<length; idx++) {
    ceil = Math.ceil(pos);
    ceil_data = src[ceil];

    floor = Math.floor(pos);
    floor_data = src[floor];

    tmp_value = (ceil_data-floor_data) * (pos-floor) + floor_data;
    conv_data[idx] = Math.round((tmp_value + 1.0) / 2.0 * 255.0);
    pos = pos + coff;
  }

  return conv_data;
}


/**
 * [onaudioprocess description]
 * @param  {[type]} event
 * @return {[type]}
 */
AudioMonitor.prototype.onaudioprocess = function(event)
{
  var monitableVadData = null;
  var audio_data = event.inputBuffer.getChannelData(0) || new Float32Array(this.analyser.frequencyBinCount);
  var int16max = 32767;
  var conv_data = null;
  var send_data = null;

  // can send client
  if ( this.client.isSendable() ) {
    conv_data = this.convertSamplingRate(audio_data, this.sampleRate, 16000);

    if ( conv_data.length > 0 ) {
      send_data = new Int16Array(conv_data.length);
      for ( var i=0, length=conv_data.length; i<length; i++ ) {
        send_data[i] = Math.round(conv_data[i] * int16max);
      }

      var tmp_data = new Uint8Array(send_data.buffer);
      this.client.sendDataFrame(tmp_data);
      tmp_data = null;
    }
  }

  if ( this.client.isCommunicable() ) {
    monitableVadData = this.client.getMonitableVadData();

    if ( monitableVadData != null ) {
      var epd_state = this.epdState & this.uiEpdState;
      conv_data = this.convertSamplingRateAndUINT8(monitableVadData, 16000, this.sampleRate);
      this.addAudioData( new AudioData(conv_data, this.vadState, epd_state) );

    } else {
      // Do Nothing
    }

  } else {
    var epd_state = this.epdState & this.uiEpdState;
    conv_data = this.convertSamplingRateAndUINT8(audio_data, this.sampleRate, this.sampleRate);
    this.addAudioData( new AudioData(conv_data, this.vadState, epd_state) );
  }

  // release resources
  send_data = null;
  conv_data = null;
  rcv_data = null;
  audio_data = null;
  monitableVadData = null;
};


/**
 * set NICTmmcv input data
 * @param {AudioData} data: audio data
 */
AudioMonitor.prototype.addAudioData = function(data)
{
  // if audio_buffer size
  this.audio_que.push(data);

  // delete unused data
  var length = Math.floor(this.vad_datasize / NUM_OF_INTERVAL);
  while ( this.audio_que.length > length ) {
    val = this.audio_que.shift();
    val = null;
  }
};


/**
 * get NICTmmcv input data
 * @return {Array(AudioData)} stored audio data
 */
AudioMonitor.prototype.getAudioData = function()
{
  return this.audio_que.splice(0, this.audio_que.length);
}


/**
 * calc audiomonitor background color
 * @param  {LevelData} level [description]
 * @return {Array} background color
 */
AudioMonitor.prototype.getBackgroundColor = function(level)
{
  var color = DEFAULT_COLOR;
  if(level.epd) {
    color = VAD_OFF_COLOR;

  } else if (level.vad) {
    color = TALKING_COLOR;

  } else {
    color = DEFAULT_COLOR;
  }
  return color;
};


/**
 * cakc audiomonitor line color
 * @param  {LevelData} level [description]
 * @return {Array} line color data
 */
AudioMonitor.prototype.getAudioLevelColor = function(level)
{
  var color = DEFAULT_COLOR;
  if(level.epd) {
    color = VAD_OFF_LEVEL_COLOR;

  } else if (level.vad) {
    color = TALKING_LEVEL_COLOR;

  } else {
    color = DEFAULT_LEVEL_COLOR;
  }
  return color;
};


/**
 * [animation description]
 * @return {[type]}
 */
AudioMonitor.prototype.animation = function()
{
  var check_time = new Date().getTime();
  var diff = check_time - this.animation_time;
  if ( diff > 1.0 / 50.0 * 1000 ) {
    this.animation_time = check_time;
    this.drawLevel();
  }

  requestAnimationFrame(this.animation.bind(this));
};


/**
 * [drawLevel description]
 * @param  {[type]} context [description]
 * @return {[type]}         [description]
 */
AudioMonitor.prototype.drawLevel = function()
{
  // convert level data
  var audio_data = this.getAudioData();

  for ( var i=0; i<audio_data.length; i++ ) {
    this.updateLevels(
      audio_data[i].audio_data,
      audio_data[i].vad_state,
      audio_data[i].epd_state,
      NUM_OF_INTERVAL
    );
  }

  this.context.clearRect(0, 0, this.width, this.height);
  var length = this.levels.length;

  if ( length > 0 ) {
    var start_col = 0;
    var prev_color = this.getBackgroundColor(this.levels[0]);
    var curr_color = prev_color;

    // draw background
    for (var i=1; i<length; i++) {
      prev_color = curr_color;
      curr_color = this.getBackgroundColor(this.levels[i]);

      if ( prev_color != curr_color || i == length-1 ) {
        this.context.fillStyle = prev_color;
        var start = this.width - i;
        var width = i - start_col;
        this.context.fillRect(start, 0, width, this.height);
        start_col = i;
      }
    }

    prev_color = this.getAudioLevelColor(this.levels[0]);
    curr_color = prev_color;

    this.context.beginPath();
    this.context.strokeStyle = curr_color;
    this.context.lineWidth = 2;

    for ( var i=0; i<length; i++ ) {
      // renew path color
      prev_color = curr_color;
      curr_color = this.getAudioLevelColor(this.levels[i]);

      if (prev_color != curr_color) {
        this.context.stroke();
        this.context.beginPath();
        this.context.strokeStyle = curr_color;
      }

      var x = this.width - i;
      var max_value = (this.levels[i].max_value-127) / 255.0;
      var min_value = (this.levels[i].min_value-127) / 255.0;
      var y_min = Math.round(min_value * this.height + this.height/2);
      var y_max = Math.round(max_value * this.height + this.height/2);

      // push level data
      this.context.moveTo(x, y_max+1);
      this.context.lineTo(x, y_min);
    }
    this.context.stroke();
  }

  // release resource
  audio_data = null;
};


/**
 * update levels
 * @param  {Array} levels     [description]
 * @param  {Uint8array} level_data [description]
 * @param  {Bool} vad_state  [description]
 * @param  {Bool} epd_state  [description]
 * @return {Array}            [description]
 */
AudioMonitor.prototype.updateLevels = function(level_data, vad_state, epd_state, interval)
{
  // calc division per level
  var div = Math.floor(level_data.length / interval);

  for (var i = 0; i < interval; i++) {
    var min_value = 128;
    var max_value = 0;

    // calcurate max value and min value
    var start = i * div;
    var end = (i+1) * div;

    for ( var idx=start; idx<end; idx++ ) {
      if (level_data[idx] < min_value) min_value = level_data[idx];
      if (level_data[idx] > max_value) max_value = level_data[idx];
    }

    var level = this.getLevelBuffer();
    level.min_value = min_value;
    level.max_value = max_value;
    level.epd = !epd_state;
    level.vad = vad_state;

    this.levels.unshift(level);
    while ( this.levels.length > this.width ) {
      var tmp = this.levels.pop();
      tmp = null;
    }
  }
};
