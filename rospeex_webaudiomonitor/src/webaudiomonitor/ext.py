# -*- coding: utf-8 -*-


class WebAudioMonitorException(Exception):
    """ WebAudioMonitorException class """
    pass


class SPIConnectorException(WebAudioMonitorException):
    """ SPIConnectorException class """
    pass


class EPDConnectorException(SPIConnectorException):
    """ EPDConnectorException class """
    pass


class AudioConnectorExcetption(SPIConnectorException):
    """ AudioConnectorExcetption class """
    pass
