#!/usr/bin/env python
# -*- coding: utf-8 -*-

# python libraries
import sys
import logging

# ros libraries
import rospy
from rospeex_msgs.msg import SpeechSynthesisRequest
from rospeex_msgs.msg import SpeechRecognitionResponse

# local libraries
import logging_util
from webaudiomonitor_server import WebAudioMonotorServer
from spi_connector import SPIConnector


logger = logging_util.get_logger(__file__)


class WebAudioMonitorNode(object):
    """ WebAudioMonitorNode class """
    SS_REQUEST_TOPIC_NAME = 'ss_req'
    SR_RESPONSE_TOPIC_NAME = 'sr_res'

    def __init__(self):
        """ init class

        """
        self._ws_host = '127.0.0.1'
        self._ws_port = 9000
        self._vad_host = '127.0.0.1'
        self._vad_port = 5002
        self._epd_host = '127.0.0.1'
        self._epd_port = 16001
        self._ws_server = None
        self._spi_connector = None

    def _sr_response_received(self, request):
        logger.info('receive sr response.')
        self._ws_server.notify_sr_text(request.message)

    def _ss_request_received(self, request):
        logger.info('receive ss request.')
        self._ws_server.notify_ss_text(request.message)

    def _load_parameter(self):
        self._ws_host = rospy.get_param('~ip_addr', self._ws_host)
        self._ws_port = rospy.get_param('~websocket_port', self._ws_port)
        self._vad_host = rospy.get_param('~ip_addr', self._vad_host)
        self._vad_port = rospy.get_param('~send_port', self._vad_port)
        self._epd_host = rospy.get_param('~ip_addr', self._epd_host)
        self._epd_port = rospy.get_param('~recv_port', self._epd_port)

    def run(self):
        # init rospy
        rospy.init_node('webaudiomonitor', anonymous=True)
        self._load_parameter()

        # subscribe topics
        rospy.Subscriber(
            self.SS_REQUEST_TOPIC_NAME,
            SpeechSynthesisRequest,
            self._ss_request_received
        )

        rospy.Subscriber(
            self.SR_RESPONSE_TOPIC_NAME,
            SpeechRecognitionResponse,
            self._sr_response_received
        )

        # setup logger
        logging_util.setup_logging(
            default_level=logging.INFO
        )

        # create websocket
        logger.info('create websocket server')
        self._ws_server = WebAudioMonotorServer(
            host=self._ws_host,
            port=self._ws_port,
            debug=True
        )

        # create spi connector
        logger.info('create spi connector')
        self._spi_connector = SPIConnector(
            vad_host=self._vad_host,
            vad_port=self._vad_port,
            epd_host=self._epd_host,
            epd_port=self._epd_port
        )

        # setup callback
        logger.info('setup callback')
        self._spi_connector.set_vad_callback(
            vad_connected=self._ws_server.notify_vad_connected,
            vad_disconnected=self._ws_server.notify_vad_disconnected
        )

        self._spi_connector.set_recv_callback(
            vad_received=self._ws_server.send_packet,
            epd_received=self._ws_server.notify_epd_message
        )

        self._ws_server.set_websocket_callback(
            recv_audio_cb=self._spi_connector.send_vad_packet,
            recv_command_cb=self._spi_connector.send_epd_packet
        )

        # start servers
        try:
            logger.info('start spi connector')
            self._spi_connector.start()

            logger.info('start websocket server')
            self._ws_server.start()

            rospy.spin()

        except rospy.ROSInterruptException:
            pass

        except Exception as err:
            logger.exception(err)

        finally:
            # end spi connector
            logger.info('end spi connector')
            self._spi_connector.end()
            self._spi_connector.join(1.0)

            # end websocket
            logger.info('end spi websocket server')
            self._ws_server.stop()


def main():

    # start web audio monitor
    node = WebAudioMonitorNode()
    node.run()
    return 1

if __name__ == '__main__':
    sys.exit(main())
