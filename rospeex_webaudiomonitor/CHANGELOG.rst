^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package rospeex_webaudiomonitor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.14.7 (2016-03-30)
-------------------

2.14.6 (2016-01-14)
-------------------

2.14.5 (2016-01-14)
-------------------

2.14.4 (2015-12-01)
-------------------

2.14.3 (2015-12-01)
-------------------
* Fix websocket connection problem

2.14.2 (2015-11-26)
-------------------
* Update rospeex_webaudiomonitor/CMakeLists.txt

2.14.1 (2015-09-18)
-------------------
* Update rospeex_webaudiomonitor/CMakeList.txt

2.14.0 (2015-09-18)
-------------------
* Switch websocket library python-autobahn to python-tornado

2.13.0 (2015-09-14)
-------------------
* Change filenames.
* Fixed difference of python-autobahn version (rosdistro)
* Added python-autobahn to rospeex_webaudiomonitor/package.xml.
* Fixed setup.py packages
* Fixed rospeex_webaudiomonitor package
* Fixed launch files
* Replace requests library with httplib2 library.

2.12.6 (2015-04-24)
-------------------

2.12.5 (2015-03-03)
-------------------

2.12.4 (2015-02-19)
-------------------

2.12.3 (2015-02-13)
-------------------
* fix issue #63

2.12.2 (2015-01-07)
-------------------

2.12.1 (2014-12-26)
-------------------
* fix package.xml
* fix comment and modify file layout.
* fix wave display
* Modify default re-connect state. manual → auto reconnect
* fix cookie issue
* fix issue #55
* commit web interface
* fix user interface
* fix input form positions.
* remove audio buffer
* fix animation samplingrate
* fix issue #53
* fix issue #51
* fix web audiomonitor
* modify lanch files, and rospeex_web audiomonitor
* fix install script
* fix filenames / add audiomonitor
* add rospeex_webaudiomonitor
